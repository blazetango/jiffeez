namespace :add_anid do
	
	@limit = 10000
	
  task :update_old_items => :environment do

        @items = Item.find(:all, :limit => @limit)
        @items.each do |item|
                #logger.debug "item product id #{item.product_id} and anid #{Product.lookup_anid(item.product_id)}"
								item.anid = Product.lookup_anid(item.product_id)
								item.save
								
        end
	end
	
	task :items_count => :environment do
		
		puts "Currently there are #{Item.count} items"
	
	end

end

