namespace :cron_job do

	desc "Task to decay ranks at the end of the day!"
	task :decay_rank => :environment do
		@all_favorites = Favorites.all
		@all_favorites.each do |favorite|
			begin
				rank = favorite.rank 
				favorite.rank = (rank.to_f * ENV['DECAY_CONSTANT'].to_f).to_s
				favorite.save
			rescue => e 
				puts e 
			end
		end
	end	

		desc "Demo task for schedular check"
		task :demo => :environment do
				File.open('schedular.txt', 'a') { |file| 
					file << DateTime.now 
					file << "\n"
				}
		end

end
