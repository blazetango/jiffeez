# To change this template, choose Tools | Templates
# and open the template in the editor.

module SmsMsg
    def self.send (from_num, to_num, message)

			Rails.logger.debug  "Sending SMS (#{Rails.configuration.jiffeez_send_sms}): from: #{from_num}, to: #{to_num}, msg: #{message}"
			
			message = URI.escape(message).gsub("(", "\(").gsub(")","\)")			
			
			if (Rails.configuration.jiffeez_send_sms.nil? || Rails.configuration.jiffeez_send_sms == true)
				cmd =  "curl -k -d From=#{from_num} -d To=#{to_num} -d Body=" + message + " https://jiffeez:545406214b7a5a4ebfeb7c47e478b44c9e2f6578@twilix.exotel.in/v1/Accounts/jiffeez/Sms/send"
				Rails.logger.debug cmd
				system cmd + " &"
			end

		end
end
