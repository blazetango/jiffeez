# load the gem
require 'spreadsheet'

# In this example the model MyFile has_attached_file :attachment
@workbook = Spreadsheet.open(Dataschema.data_import)

# Get the first worksheet in the Excel file
@worksheet = @workbook.worksheet(0)

# It can be a little tricky looping through the rows since the variable
# @worksheet.rows often seem to be empty, but this will work:
0.upto @worksheet.last_row_index do |index|
  # .row(index) will return the row which is a subclass of Array
  #@contact = Contact.new
  #row[0] is the first cell in the current row, row[1] is the second cell, etc...
  #@contact.first_name = row[0]
  #@contact.last_name = row[1]
  
  
  row = @worksheet.row(index)
  @prodrow = Dataschema.new
  @prodrow.last_row_index do |index|
	@prodrow.category = row[0]
	@prodrow.subcategory = row[1]
	@prodrow.product = row[2]
	@prodrow.brand = row[3]
	@prodrow.variant = row[4]
	@prodrow.size = row[5]	
	@prodrow.mrp = row[6]	
	@prodrow.sellprice = row[7]	
	@prodrow.uom = row[8]	
  @prodrow.save
  end
end
