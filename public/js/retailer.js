function Order(e) {
    var t = this;
    var n = function (e) {
        var n = {};
        for (var r = 0; r < e.length; r++) {
            e[r].subtotal = e[r].item_uom != null && e[r].item_uom != "pc" && e[r].item_uom.length < 10 ? parseFloat(e[r].quantity) * parseFloat(e[r].merchant_price) / parseFloat(getridofalphabets(e[r].item_size)) : parseFloat(e[r].quantity) * parseFloat(e[r].merchant_price);
            if (e[r].item_subcateg_name in n) n[e[r].item_subcateg_name][e[r].product_id] = e[r];
            else {
                n[e[r].item_subcateg_name] = new Object;
                n[e[r].item_subcateg_name][e[r].product_id] = e[r]
            }
            t.total += e[r].subtotal
        }
        t.merchant_total = t.total;
        return n
    };
    this.data = e;
    this.total = 0;
    this.merchant_total = 0;
    this.groupedData = n(e.items);
    this.removeItem = function (e, t) {
        logCall(arguments);
        var n = this.groupedData[t][e];
        this.total -= n.subtotal;
        n.is_retailer_removed = true;
        --this.data.cart.items_count
    };
    this.addItem = function (e, t) {
        logCall(arguments);
        var n = this.groupedData[t][e];
        this.total += n.subtotal;
        n.is_retailer_removed = false;
        ++this.data.cart.items_count;
    };
    
    this.updateItemFinalPrice = function (e, t, n) {
        logCall(arguments);
        var r = this.groupedData[e][t];
        var i = r["quantity"];
        
        if(r["item_uom"] == "100g")

        {
        	i = r["quantity"]/100;
        }
        
        if (!isNaN(i)) {
            this.total -= r.subtotal;
            r.final_price = n;

            r.subtotal = i * n;
			console.log( r.subtotal);
            this.total += r.subtotal;
        }
    }
}
function waitIconStart(e) {
    logCallStart(arguments);
    jQuery("body").addClass("ui-loading");
    if (e) {
        $(e).addClass("ui-disabled")
    }
    logCallEnd(arguments)
}
function waitIconStop(e) {
    logCallStart(arguments);
    jQuery("body").removeClass("ui-loading");
    if (e) {
        $(e).removeClass("ui-disabled");
        if ($.mobile.sdCurrentDialog) {
            $.mobile.sdCurrentDialog.close()
        }
    }
    logCallEnd(arguments)
}
function msg_after_add(e, t, n) {
    logCall(arguments);
    var r = n + t + " in cart";
    return r
}
function msg_after_add_brand(e, t, n) {
    logCall(arguments);
    var r = "In the cart: " + Math.round(n);
    return r
}
function msg_after_add_cancel_button(e, t, n, r) {
    logCall(arguments);
    var i = '<a href="#itempage" onclick=\'Removefromcart(' + e + "," + r + ',"' + n.toHtml() + '","' + t + '")\'><img width="24" src="/cancel.png"/></a>';
    return i
}
function msg_after_add_cancel_button_multiple_sizes(e, t, n, r, i) {
    logCall(arguments);
    var s = '<a href="#itempage" onclick=\'RemovefromcartMultipleSizes(' + e + "," + t + "," + i + ',"' + r.toHtml() + '","' + n + '")\'><img width="24" src="/cancel.png"/></a>';
    return s
}
function msg_for_add_nonbrand(e, t, n, r) {
    logCall(arguments);
    var i = '<a href="#itempage" onclick=\'Removefromcart(' + e + "," + r + ',"' + n + '","' + t + '")\'><img width="24" src="/cancel.png"/></a>';
    document.getElementById(e + "b").innerHTML = r + n + " in cart";
    document.getElementById(e + "c").innerHTML = i;
    return
}
function msg_for_add_nonbrand_favorite(e, t, n, r, i) {
    logCall(arguments);
    var s = '<a href="#favorites_page" onclick=\'RemovefromcartFromFavorites(' + e + "," + r + ',"' + n + '","' + t + '")\'><img width="24" src="/cancel.png"/></a>';
    document.getElementById(e + "sb").innerHTML = r + n + " in cart";
    document.getElementById(e + "sc").innerHTML = s;
    if (i == "favorite") {
        $("#myFavoriteItemsList").children("li").children(".container_16").find("#" + e + "sb").html(r + n + " in cart");
        $("#myFavoriteItemsList").children("li").children(".container_16").find("#" + e + "sc").html(s)
    }
    return
}
function msg_for_add_brand(e, t, n, r) {
    logCall(arguments);
    var i = '<a href="#itempage" onclick=\'Removefromcart(' + e + "," + r + ',"' + n + '","' + t + '")\'><img width="24" src="/cancel.png"/></a>';
    document.getElementById(e + "b").innerHTML = "In the cart: " + r.toFixed(0);
    document.getElementById(e + "c").innerHTML = i;
    return
}
function msg_for_add_product_delete(e, t, n, r) {
    logCall(arguments);
    $("#myFavoriteItemsList").children("li").children(".container_16").find("#" + e + "sb").addClass("demod sfds");
    var i = '<a href="#" onclick=\'RemovefromcartFromFavorites(' + e + "," + r + ',"' + n + '","' + t + '")\'><img width="24" src="/cancel.png"/></a>';
    document.getElementById(e + "sb").innerHTML = "In the cart: <span class='c_" + e + "'>" + r.toFixed(0) + "</span>";
    document.getElementById(e + "sc").innerHTML = i;
    return
}
function msg_for_add_brand_multiple_sizes(e, t, n, r, i) {
    logCall(arguments);
    var s = '<a href="#itempage" onclick=\'RemovefromcartMultipleSizes(' + e + "," + t + "," + i + ',"' + r + '","' + n + '")\'><img width="24" src="/cancel.png"/></a>';
    document.getElementById(e + "b").innerHTML = "In the cart: " + i;
    document.getElementById(e + "c").innerHTML = s;
    return
}
function msg_for_noadd(e) {
    logCall(arguments);
    document.getElementById(e + "b").innerHTML = "No items added";
    return
}
function msg_for_noadd_favorite(e) {
    logCall(arguments);
    document.getElementById(e + "sb").innerHTML = "No items added";
    return
}
function msg_after_add_3(e, t, n, r, i, s, o) {
    logCall(arguments);
    var u = '<a href="#shoppingcart_page" ' + o + ' class="removeFromCartIcon1"><img width="30" src="/cancel.png"/></a>';
    return u
}
function msg_for_noadd_cart(e) {
    logCall(arguments);
    document.getElementById(e + "sb").innerHTML = "No items added";
    return
}
function showRetailerCart(e, t) {
    logCall(arguments);
    cookiecartid = e;
    var n;
    var r = 0,
        i = finaltotalbill = 0,
        s;
    var o = document.createElement("div");
    var u;
    var a;
    var f = 0;
    var l = unavilable = "";
    var c;
    var h = 0;
    totalSaving = 0;
    mrpSubTotalArr = [];
    var p = retailerNote = "";
    $.mobile.changePage("#retailer_cart_page");
    $("#retailer_purchaseditemlist").empty();
    $("#retailer_purchaseditemlistdiv").empty();
    $.get("/shoppingcarts/get_cart_details?cart_id=" + e, function (r) {
        finaltotalbill = r.cart.final_price;
        retailerNote = r.cart.retailer_note;
        if (finaltotalbill != null && finaltotalbill != "null" && finaltotalbill != r.cart.merchant_price) {
            finaltotalbill = " " + parseFloat(finaltotalbill).toFixed(2);
            p = 'style="text-decoration:line-through"'
        } else {
            finaltotalbill = "";
            p = ""
        } if (retailerNote != null) {
            retailerNote = retailerNote
        } else {
            retailerNote = "Comment Here.."
        }
        order = new Order(r);
        c = order.data.cart.state;
        var f = order.data.cart.delivery_time;
        var d = "";
        if (f) {
            d = getStandredDateArr(f)
        }
        u = "<h4>Pending Order #" + order.data.cart.short_id + ": Rs." + parseFloat(order.data.cart.final_price).toFixed(2) + " COD</h4>" + "<span class='caption'>Delivery Address: </span>" + order.data.cart.delivery_address + "<br/>" + "<span class='caption'>Delivery Time: </span>" + d + "<br/>";
        allitem = order.data.items;
        u += '<ul data-role="listview" id="retailer_purchaseditemlist" data-autodividers="true" data-inset="true" data-filter="true">';
        Totalbill = 0;
        var v = 0;
        Utils.forEachSorted(order.groupedData, function (t) {
            subtabdata = order.groupedData[t];
            u += "<li data-role='list-divider'>" + t + "</li>";
            for (var o in subtabdata) {
                n = subtabdata[o];
                var f = horizontalLineMerchant = "";
                var p = n.final_price;
                var d = n.is_retailer_removed;
                if (p != null && p != "null" && p != r.items[v].merchant_price) {
                    p = parseFloat(p).toFixed(1);
                    horizontalLineMerchant = 'style="text-decoration:line-through"';
                    f = n["item_uom"] != null && n.item_uom != "pc" && n["item_uom"].length < 10 ? parseFloat(n["quantity"]) * parseFloat(p) / parseFloat(getridofalphabets(n["item_size"])) : parseFloat(n["quantity"]) * parseFloat(p)
                } else {
                    p = horizontalLineMerchant = f = "";
                    if (!d) {
                        h = n["item_uom"] != null && n.item_uom != "pc" && n["item_uom"].length < 10 ? parseFloat(n["quantity"]) * parseFloat(n.mrp) / parseFloat(getridofalphabets(n["item_size"])) : parseFloat(n["quantity"]) * parseFloat(n.mrp);
                        if (h > n.subtotal) {
                            mrpSubTotalArr[v] = h - n.subtotal;
                            totalSaving = totalSaving + (h - n.subtotal)
                        }
                        else
                        {
                        	mrpSubTotalArr[v] = 0;
                            totalSaving = totalSaving + 0;
                        }
                    }
                } if (d) {
                    l = 'style="width:55%;opacity:0.5;"';
                    unavilable = "style='opacity: 0.5;margin:7px; float: left; width: 40%; display:block'"
                } else {
                    l = 'style="width:55%;margin-top:2px;"';
                    unavilable = "style='opacity: 0.5;margin:7px; float: left; width: 40%;display:none'"
                } if (n["item_subcateg_name"] in order.groupedData) {
                    if (n["product_id"] in order.groupedData[n.item_subcateg_name]) {
                        if (n.item_uom != null && n.item_uom != "pc" && n.item_uom.length < 10) msg1 = msg_after_add(n.product_id, getridofnumbers(n.item_size), order.groupedData[n.item_subcateg_name][n.product_id].quantity);
                        else msg2 = msg_after_add_brand(n.product_id, getridofnumbers(n.item_size), order.groupedData[n.item_subcateg_name][n.product_id].quantity)
                    }
                }
                if (c != "checked_out") {
                    add_attr = 'style="display:none"'
                } else {
                    retailerNote = "Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP";
                    add_attr = ""
                }
                s = msg_after_add_3(n.product_id, n.item_subcateg_name, "1pc", n.quantity, v, e, add_attr);
                i = n.subtotal;
                a = "  " + n["quantity"] + " " + (n["item_uom"] != null && n.item_uom != "pc" && n["item_uom"].length < 10 ? getridofnumbers(n["item_size"]) : "pc(s)") + ", &#8377; <span " + horizontalLineMerchant + "  class='subtotal1_old' >" + parseFloat(i.toFixed(2)) + "</span> <span class='subtotal1_new'> " + f + "</span>";
                u += "<li data-anid='" + n.product_id + "' data-subname='" + n.item_subcateg_name + "'>" + "<div class='container_16'><div class='grid_14'>" + n.product_id + ": " + nonull(n.product_name) + " " + nonull(n.variant_name) + "</div>" + s + "<div class='grid_8' style='width:90%;'><span class='order_value1'>" + n.item_size + " | &#8377; <span " + horizontalLineMerchant + " class='old_value1'>" + n.merchant_price + "</span><span class='new_value1'> " + p + "</span></span><img width='30' src='http://180.149.246.126/Rahul/jiffeez/Edit_icon.png' " + add_attr + " class='edit_icon_1' /><span class='edit_content'><img width='35' src='http://180.149.246.126/Rahul/jiffeez/button-check_green.png' qty='" + n.quantity + "' counter1='" + v + "' data-cartid='" + e + "' class='check_icon_1' /><input type='text' width='30' class='edit_value_1'>" + "<img width='30' src='/cancel.png' class='delete_icon_1' /></span></div><div class='grid_2' id='" + n.product_id + "sc'></div><div class='grid_6' " + l + " id='" + n.product_id + "sb'>" + a + "</div><span class='undo_action'></span><div class='unavailable' " + unavilable + " >Unavailable</div></div></li>";
                Totalbill += i;
                v++
            }
        });
        u += "</ul>";
        u += "<br/><h3 id='h3' align='right' style='background-color:white; border: solid 5px #99aabb;'>Total :&#8377;<span class='totalbill_1_old' " + p + ">" + parseFloat(Totalbill).toFixed(2) + "</span><span class='totalbill_1_new'>" + finaltotalbill + "</span></h3><input type='text' value='" + retailerNote + "' class='retailer_comment_1' />";
        if (t == 1) u += "<a data-role='button' data-href='/shoppingcarts/processOrder?cart_id=" + e + "&total=" + parseFloat(Totalbill).toFixed(2) + "' class='clsBtnRetailerConfirm' data-icon='check' data-iconpos='right' data-theme='e'>Confirm</a>";
        if (Totalbill != 0) o.innerHTML = u;
        $("#retailer_purchaseditemlistdiv").append(o);
        jQuery("#retailer_cart_page").trigger("pagecreate")
    }, "JSON")
}
function retailer_order() {
    logCall(arguments);
    var e;
    waitIconStart();
    $("#pending").empty();
    $("#in-process").empty();
    $("#completed").empty();
        //added by mandar
        var a = "<li><span>Sent</span><span style='margin-left: 1%;'>Delivered</span></li>";
	$("#in-process").append(a);
    var t = {};
    var n;
    $.get("/shoppingcarts/getorders", function (r) {
        getorders=r;
        for (var i = 0; i < r.length; i++) {
            if (r[i].state in t) t[r[i].state]++;
            else t[r[i].state] = 1;
            n = "Unknown";
            if (r[i].delivery_time) {
                n = getStandredDateArr(r[i].delivery_time)
            }
            if (r[i].state == "checked_out") {
                e = "<li> <a href='#' onclick='showRetailerCart(\"" + r[i].id + "\",1)'>" + n + " | Order #" + r[i].short_id + " | Rs. " + parseFloat(r[i].final_price).toFixed(2) + " COD | " + r[i].delivery_address + " </a></li>";
                $("#pending").append(e)
            } 
// changes done by mandar 
            else if (r[i].state == "in-process"){
                var flag = ""
                if (r[i].is_sent)                
                  flag = "checked";                 
                else
                  flag = "";

                e = "<li><label><input type='checkbox' name='checkbox-2' id='checkbox-2' class='process_dilevery'  value='"+ r[i].id + "'style='width:4%; margin:15px 0;'"+  flag + " /></label><label><input type='checkbox' name='checkbox-2' id='checkbox-2' value='"+ r[i].id +"'class='process_completed'  style='width:4%;'/></label><a href='#' class='ui-link-inherit' style='display:inline;' onclick=\"showRetailerCart('" + r[i].id + "',0)\">" + n + " |  Order #" + r[i].short_id + " | Rs. " + parseFloat(r[i].final_price).toFixed(2) + " COD | " + r[i].delivery_address + " </a></li>";
              //  e = "<li> <a href='#' onclick=\"showRetailerCart('" + r[i].id + "',0)\">" + n + " |  Order #" + r[i].short_id + " | Rs. " + parseFloat(r[i].final_price).toFixed(2) + " COD | " + r[i].delivery_address + " </a></li>";
                $("#in-process").append(e);
            }
            else
            {
              //e = "<li> <a href='#' onclick='showRetailerCart(\"" + r[i].id + "\",1)'>" + n + " | Order #" + r[i].short_id + " | Rs. " + parseFloat(r[i].final_price).toFixed(2) + " COD | " + r[i].delivery_address + " </a></li>";
              e = "<li> <a href='#' onclick='showRetailerCart(\"" + getorders[i].id + "\",1)'>" + n + " | Order #" + getorders[i].short_id + " | Rs. " + parseFloat(getorders[i].final_price).toFixed(2) + " COD | " + getorders[i].delivery_address + " </a> <span id=" + getorders[i].id + " class='undo_action_1'></span></li>";

              $("#completed").append(e);
            }
        }
        for (str in t) if (str == "checked_out") $("#pending_count").text(t[str]);
            else if (str == "in-process") $("#in-process_count").text(t[str]);
        waitIconStop();
        $("#pending").listview("refresh");
        $("#in-process").listview("refresh");
        $("#completed").listview("refresh")
        $("#completed_count").text($("#completed").find("li").size());
    }, "JSON")
}
// changes done by mandar 
$(".process_dilevery").live("click",function(){
console.log(this.checked);
});

var compl_ords= [];
$(".process_completed").live("click",function(){
id = $(this).val();
  for (var i = 0; i < getorders.length; i++) {
  	if(getorders[i].id == id)
  	  {
  		 if (getorders[i].delivery_time) {
        n = getStandredDateArr(getorders[i].delivery_time)
       }
  		if(this.checked){
  		  e = "<li> <a href='#' onclick='showRetailerCart(\"" + getorders[i].id + "\",1)'>" + n + " | Order #" + getorders[i].short_id + " | Rs. " + parseFloat(getorders[i].final_price).toFixed(2) + " COD | " + getorders[i].delivery_address + " </a> <span id=" + getorders[i].id + " class='undo_action_1'></span></li>";
       // changes done by mandar 
 $.post("/retailers/order_delivered",{"cart_id": $(this).val()},function(){
          //alert(window.location.href);
          
        });
        $("#completed").append(e);
        compl_ords.push(getorders[i])
       // changes done by mandar  
  		}
  		else{}
  		//compl_ords.push(getorders[i])
  	}
  }

	$(this).closest("li").remove();

	$("#in-process").listview("refresh");
// changes done by mandar 
	$("#in-process_count").text($("#in-process").find("li").size()-1);

	$("#completed").listview("refresh");
	$("#completed_count").text($("#completed").find("li").size());

});

$(".undo_action_1").live("click", function(){
	var id = $(this).attr("id");
	 for (var i = 0; i < getorders.length; i++) {
		if(getorders[i].id == id)
		{
			 if (getorders[i].delivery_time) {
                n = getStandredDateArr(getorders[i].delivery_time)
			}
			 e = "<li><label><input type='checkbox' name='checkbox-2' id='checkbox-2' class='process_dilevery'  value='"+ getorders[i].id +"' style='width:4%; margin:15px 0;' /></label><label><input type='checkbox' name='checkbox-2' id='checkbox-2' value='"+ getorders[i].id +"' class='process_completed'  style='width:4%;'/></label><a href='#' class='ui-link-inherit' style='display:inline;' onclick=\"showRetailerCart('" + getorders[i].id + "',0)\">" + n + " |  Order #" + getorders[i].short_id + " | Rs. " + parseFloat(getorders[i].final_price).toFixed(2) + " COD | " + getorders[i].delivery_address + " </a></li>";
     // changes done by mandar 
 $.post("/retailers/undo_order_delivery",{"cart_id": getorders[i].id },function(){
       //alert(window.location.href);
      });   
       $("#in-process").append(e)
		}
	 }
	$(this).closest("li").remove();

	$("#in-process").listview("refresh");
	$("#in-process_count").text($("#in-process").find("li").size());

	$("#completed").listview("refresh");
	$("#completed_count").text($("#completed").find("li").size());
});




function fill_completed_orders()
{
	for (var i = 0; i < compl_ords.length; i++) {
		if (compl_ords[i].delivery_time) {
                n = getStandredDateArr(compl_ords[i].delivery_time)
           }
		 e = "<li> <a href='#' onclick='showRetailerCart(\"" + compl_ords[i].id + "\",1)'>" + n + " | Order #" + compl_ords[i].short_id + " | Rs. " + parseFloat(compl_ords[i].final_price).toFixed(2) + " COD | " + compl_ords[i].delivery_address + " <span class='undo_action' style='display: inline;float: right; margin-top: -30px;'></span> </a></li>";
         $("#completed").append(e)
		 
	 }
	$("#completed").listview("refresh");
	$("#completed_count").text($("#completed").find("li").size());
}
//changes by mandar end

var order;
$(".removeFromCartIcon1").live("click", function () {
    var e = $(this).closest("li");
    var t = e.data("subname");
    var n = e.data("anid");
	
	totalSaving = totalSaving - mrpSubTotalArr[$(this).siblings(".grid_8").children(".edit_content").children(".check_icon_1").attr("counter1")];
	if (totalSaving == 0) {
		$(".retailer_comment_1").val("Comment Here..")
	} else {
		$(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
	}

    order.removeItem(n, t);
	
    $(this).siblings(".unavailable").show();
	$(this).siblings(".undo_action").show();
    $(this).siblings(".grid_6").css("opacity", "0.5");
    $(this).hide();
    $(this).addClass("ui-disabled");
	e.find('.edit_icon_1').hide();
	e.find('.edit_icon_1').addClass("ui-disabled");
    $(".totalbill_1_old").css({"text-decoration":"line-through", "color":"red"});
    $(".totalbill_1_new").html("  " + order.total.toFixed(2))
});

$(".undo_action").live("click", function () {
	var e = $(this).closest("li");
	var n = e.data("subname");
    var r = e.data("anid");
	var w = e.find('.old_value1').html();
	e.find(".edit_value_1").val("");
	
    totalSaving = totalSaving + mrpSubTotalArr[$(this).siblings(".grid_8").children(".edit_content").children(".check_icon_1").attr("counter1")];
	order.groupedData[n][r].subtotal.toFixed(1);

	var v =	e.find('.new_value1').html();

	if(v > 0)
	{
		e.find('.new_value1').html(" ");
		e.find('.old_value1').css({"text-decoration":"none", "color":"black"});
		e.find('.subtotal1_new').html(" ");
		e.find('.subtotal1_old').css({"text-decoration":"none", "color":"black"});
		order.updateItemFinalPrice(n, r, w);
	}
	else
	{
		order.addItem(r,n);
	}

	if (totalSaving == 0) {
	 $(".retailer_comment_1").val("Comment Here..")
	} else {
	 $(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
	}

	var t = e.data("subname");
    var n = e.data("anid");
 
	//order.addItem(n, t);
    $(this).hide();
	$(this).next().hide();
    $(this).siblings(".grid_6").css("opacity", "1");
    e.find('.removeFromCartIcon1').show();
	e.find('.removeFromCartIcon1').removeClass("ui-disabled");
	e.find('.edit_icon_1').show();
	e.find('.edit_icon_1').removeClass("ui-disabled");
	$(".totalbill_1_old").css({"text-decoration" : "line-through", "color":"red"});
    $(".totalbill_1_new").html("  " + order.total.toFixed(2));
    $(".totalbill_1_old").html(order.merchant_total.toFixed(2));
});


var totalSaving = 0;
var mrpSubTotalArr = [];
$(".retailer_comment_1").live("focus", function () {
    if ($(this).val() == "Comment Here..") {
        $(this).val("")
    }
});
$("#retailer_purchaseditemlist .container_16 .edit_icon_1").live("click", function () {
    $(this).hide();
    $(this).siblings(".edit_content").css("display", "inline-block")
});
$("#retailer_purchaseditemlist .container_16  .edit_content .delete_icon_1").live("click", function () {
    $(this).parent(".edit_content").siblings(".edit_icon_1").show();
    $(this).parent(".edit_content").hide()
});
$("#retailer_purchaseditemlist .container_16 .edit_content .check_icon_1").live("click", function () {
    var e = parseFloat($(this).siblings(".edit_value_1").val()).toFixed(1);
    var t = $(this).closest("li");
    if (isNaN(e)) {
        return false
    }
    var n = t.data("subname");
    var r = t.data("anid");
   
    var i = order.groupedData[n][r].subtotal.toFixed(1);
    order.updateItemFinalPrice(n, r, e);
    $(this).parent(".edit_content").siblings(".edit_icon_1").show();
    var s = $(this).parent(".edit_content").parent(".grid_8");
    s.children(".order_value1").children(".old_value1").css({"text-decoration" : "line-through", "color":"red"});
    $(this).parent(".edit_content").hide();

	t.find('.removeFromCartIcon1').hide();
	t.find('.removeFromCartIcon1').addClass("ui-disabled");
	t.find('.undo_action').show();

    s.children(".order_value1").children(".old_value1").html(order.groupedData[n][r].merchant_price);
    s.children(".order_value1").children(".new_value1").html("  " + order.groupedData[n][r].final_price);
    s.siblings(".grid_6").children(".subtotal1_new").html("  " + order.groupedData[n][r].subtotal.toFixed(1));
    s.siblings(".grid_6").children(".subtotal1_old").css({"text-decoration" : "line-through", "color":"red"});
    $(".totalbill_1_old").css({"text-decoration" : "line-through", "color":"red"});
    $(".totalbill_1_new").html("  " + order.total.toFixed(2));
    $(".totalbill_1_old").html(order.merchant_total.toFixed(2));
    
    var oldsub = i;
    var newSub = order.groupedData[n][r].subtotal.toFixed(1);

	console.log(totalSaving +"---------"+ mrpSubTotalArr[$(this).attr("counter1")]);
    totalSaving = totalSaving - mrpSubTotalArr[$(this).attr("counter1")];
    if (totalSaving <= 0) {
		$(".retailer_comment_1").val("Comment Here..")
	} else {
		$(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
	}

/*
	$(this).siblings(".undo_action").show();
    $(this).siblings(".grid_6").css("opacity", "0.5");
    $(this).hide();
    $(this).addClass("ui-disabled");
*/


	/* commented by sudik
	if(newSub > oldsub && newSub != oldsub)
    {
    	console.log('newsub' + newSub + '== old sub '+ oldsub);
    	totalSaving = totalSaving - (newSub - oldsub);
	    mrpSubTotalArr[$(this).attr("counter1")] = newSub - oldsub;
	    if (totalSaving <= 0) {
	        $(".retailer_comment_1").val("Comment Here..")
	    } else {
	        $(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
	    }
    }
    else if(newSub < oldsub && newSub != oldsub)
    {
    	console.log('newsub' + newSub + '== old sub '+ oldsub);
    	
    	totalSaving = totalSaving + (oldsub -newSub);
	    mrpSubTotalArr[$(this).attr("counter1")] = oldsub -newSub;
	    if (totalSaving <= 0) {
	        $(".retailer_comment_1").val("Comment Here..")
	    } else {
	        $(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
	    }
    }
    */
    /*
    totalSaving = totalSaving - mrpSubTotalArr[$(this).attr("counter1")];
    mrpSubTotalArr[$(this).attr("counter1")] = 0;
    if (totalSaving == 0) {
        $(".retailer_comment_1").val("Comment Here..")
    } else {
        $(".retailer_comment_1").val("Your saving is at least Rs." + totalSaving.toFixed(2) + " on MRP")
    }
    */
    
    
});
$(".clsBtnRetailerConfirm").live("click", function () {
    var e = this.baseURI;
    waitIconStart(this);
    order.data.cart.final_price = order.total;
    order.data.cart.merchant_price = order.merchant_total;
    order.data.cart.retailer_note = $(".retailer_comment_1").val();
    $.post($(this).attr("data-href"), {
        cart_details: order.data
    }, function () {
        waitIconStop();
        $.mobile.changePage(e)
    })
})
// changes done by mandar 
$('.process_dilevery').live("click",function(){
  var flag = false;
  if ($(this).attr("checked"))
    {
      flag = true
    }
  $.post("/retailers/order_sent",{"cart_id": $(this).val(), "flag" : flag},function(){
    //alert(window.location.href);
  });


});
