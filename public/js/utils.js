var isLoggingOn = true;

var logCall = function() {}, logCallStart = function() {}, logCallEnd  = function() {};

if (!isLoggingOn) {
	//console.log = function () {};
}
else {
	
	logCall = function(args) {
		var params = '';
		for (var i = 0; i < args.length; i++) {
			if (typeof args[i] === 'function') {
				params += 'function' + ',';
			}
			else if (typeof args[i] === 'string') {
				params += "'" + args[i] + "',";
			}
			else if (Array.isArray(args[i])) {
				params += 'array[' + args[i].length + '],';
			}
			else {
				params += args[i] + ',';
			}
		}
		
		if (params.length > 0)
			params = params.slice(0, -1);

		//console.log("" + new Date().getTime() + ": " + args.callee.name + "(" + params + ")");
	}
	
	logCallStart = function(args) {
		
		logCall(args);
		//console.time(args.callee.name);
	}

	logCallEnd = function(args) {

		//console.log("" + new Date().getTime() + ": exit " + args.callee.name);
		//console.timeEnd(args.callee.name);
	}
}
// Internationalization strings
var dateFormat = {};

dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// Goal: given 30/11/2012 10:30 AM 
//	output: 10:30 AM
Date.prototype.toShortTimeString = function () {
	
	if (isNaN(this.getTime())) {
		return "Unknown"
	}
	
	var hour = this.getHours();

	if (hour < 12) {
		a_p = "AM";
	}
	else {
		a_p = "PM";
	}
	
	if (hour == 0) {
		hour = 12;
	}
	else if (hour > 12) {
		hour = hour - 12;
	}

	var min = this.getMinutes() + "";
	if (min.length == 1) {
		min = "0" + min;
	}
	
	return hour + ":" + min + " " + a_p;
}

Date.prototype.toShortTimeString1 = function (hrs,minutes) {
	
	if (isNaN(this.getTime())) {
		return "Unknown"
	}
	
	var hour = hrs;

	if (hour < 12) {
		a_p = "AM";
	}
	else {
		a_p = "PM";
	}
	
	if (hour == 0) {
		hour = 12;
	}
	else if (hour > 12) {
		hour = hour - 12;
	}

	var min = minutes + "";
	if (min.length == 1) {
		min = "0" + min;
	}
	
	return hour + ":" + min + " " + a_p;
}

Date.prototype.toShortDateString = function () {
	/*
	if (isNaN(this.getTime())) {
		return "Unknown"
	}
	*/
	
	var day = this.getDay();
	var date = this.getDate();
    var month = this.getMonth(); //Months are zero based
    var year = this.getFullYear();
	
	var sup = "";
	if (date == 1 || date == 21 || date ==31) {
		sup = "st";
	}
	else if (date == 2 || date == 22) {
		sup = "nd";
	} 
	else if (date == 3 || date == 23) {
		sup = "rd";
	}
	else {
		sup = "th";
	}

	var str = dateFormat.i18n.dayNames[day] + ", " + date + "<sup>" + sup + "</sup> " + dateFormat.i18n.monthNames[month] + " " + year;
	
	return str;
}

// Input: given 30/11/2012 10:30 AM
//	output: 30 Nov 2012 10:30 AM
Date.prototype.toShortDateTimeString = function () {

	if (isNaN(this.getTime())) {
		return "Unknown"
	}
	
	return this.toShortDateString() + " " + this.toShortTimeString();
};

String.prototype.toShortDateString = function () {
	var dt = new Date(this);
	
	if (isNaN(dt.getTime())) {
		return "Unknown21"
	}

	return dt.toShortDateString();
}

String.prototype.toShortTimeString = function () {
	var dt = new Date(this);
	
	if (isNaN(dt.getTime())) {
		throw "invalid";
		return "Unknown"
	}

	return dt.toShortTimeString();
}

String.prototype.toShortDateTimeString = function () {
	var dt = new Date(this);
	
	if (isNaN(dt.getTime())) {
		return "Unknown"
	}

	return dt.toShortDateTimeString();
}

// Input: given 30/11/2012 10:30 AM, 1 hour range
//	output: 30 Nov 2012 (10:30 AM to 11:30 AM)
Date.prototype.toTimeRangeString = function (rangeHours) {

	if (isNaN(this.getTime())) {
		return "Unknown"
	}
	
	var dateTo = new Date(this);
	if (!rangeHours) rangeHours = 1;
	dateTo.setHours(this.getHours() + rangeHours);
	
	//	from = this, and to = dateTo
	var str = this.toShortDateString() + " (" + this.toShortTimeString() + " to " + dateTo.toShortTimeString() + ")";
	
	return str;
};

Date.prototype.toTimeRangeString1 = function (rangeHours) {

	var dateTo = new Date(this);
	if (!rangeHours) rangeHours = 1;
	dateTo.setHours(this.getHours() + rangeHours);
	
	//	from = this, and to = dateTo
	var str = dateTo.toShortTimeString();
	
	return str;
};

String.prototype.toTimeRangeString = function (rangeHours) {
	var dt = new Date(this);
	
	if (isNaN(dt.getTime())) {
		return "Unknown"
	}

	return dt.toTimeRangeString(rangeHours);
}

String.prototype.toHtml = function () {
    return this.replace(/&/g, '&amp;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#39;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;');
}

// Iterate sorted through an object's keys
// based on: http://stackoverflow.com/questions/890807/iterate-over-a-javascript-associative-array-in-sorted-order/890829#890829
// Usage:
// var myObj = { a:1, b:2 };
// myObj.iterateSorted(function(value) { alert(value); } 
function Utils() {}

Utils.forEachSorted = function(object, fnProcessEach) {
    var keys = [];
    for (var key in object) {
            keys.push(key);
    }
    keys.sort();

    for (var i = 0; i < keys.length; i++) {
        fnProcessEach(keys[i]);
    }
};

Utils.formatJSONDate = function(str) {
	if (str == null)
	{
			return null;
	}
	str = str.slice(0,str.indexOf("+"));
	str = str.replace("-","/");
	str = str.replace("-","/");
	str = str.replace("T"," ");
	return str;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Misc functions


//following function will convert isodateformate into standred date formate.

function getStandredDateArr(datestr,typestr)
{
	datestr = datestr.replace('T',' ');
	datestr = datestr.replace('+',' ');

	var arr = datestr.split(/[- :]/),
	date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
	
	if(typeof typestr == 'undefined')
	{
		return date.toTimeRangeString(1);
	}
	else
	{
		return date.toShortDateTimeString();
	}
}



function datemod(update)
{
	logCall(arguments);
	var formatdate,formattime,format; 
	update = new Date(update);
	formatdate = update.toLocaleDateString();
	formattime = update.toLocaleTimeString();
	format = formatdate + ", " + formattime;
	return format;
}
function getridofquotes(str){
	logCall(arguments);

	var x = str.replace(/\"/g,""); 
	x = x.replace(/null/g,"-");
	return x;
}

function nonull(str){
	logCall(arguments);

	if(str == null)									//Cant use replace() because we are passing objects not string so this way suitable
		return ""
	else
		return str;
}

function getridofalphabets(str){
	logCall(arguments);
	
	var x = str.replace(/[a-zA-Z]/g,"");
	x = x.replace(/ /g,"");
	return x;
}

function getridofnumbers(str){

	logCall(arguments);
	
	var x = str.replace(/\d+/g,"");
	x = x.replace(/:/g,"");
	x = x.replace(/ /g,"");
	return x;
}

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */
/*
var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
function changeDate123 (d,mask, utc) {
	return dateFormat(d, mask, utc);
};
*/

