var allcateg = new Object();						//Used in funccat
var mycategories = new Object();					//Used in func
var mysubcats = new Object();						//Used in funcsub
var brandobj = new Object();						//Used in funcbrand
var brandofsub = new Object();						//Used in getbrandid
var purchaseditemid = new Object();					//Used in mapping_items and in shopping
var Temp_purchaseditemid = new Object();	 		//Used to revert cancelled item in shopping cart pages
var Totalbill=0;									//Used in shoppingcart_display

//The problem is that ul element is being accessed before it exists. So we use listview=refresh

//Used to print the categories list
function print_categ_link(category){
	$('#ref_for_categ').empty();					//Used to clear the category list to avoid repeatation
	
	var get,each_category,j;
		get = document.createElement("div");
//		htmlSource = '<div class="ui-grid-a"><div class="ui-block-a">';
		htmlSource = '<div class="container_12">';
	for(var i=0;i<category.length;i++){
		each_category = category[i];
		j=i+1;
//		if(j<9)
//		htmlSource = htmlSource + '<a data-role="button"  data-transition="slide" id="ref_for_categ" data-theme="d" href="#itempage" onclick="funcsub(' + j + ')" ><div class="ui-grid-d"><div class="ui-block-a"><img src="/Categpics/'+ j +'.png" id="catimg" height=36 class="align-left"/></div><div class="text-align-right">' + getridofquotes(JSON.stringify(each_category['categname'])) + '</div></div></a>';
//		else if(j==9)
//		htmlSource += '</div><div class="ui-block-b"><a data-role="button" data-transition="slide" id="ref_for_categ" data-theme="d" href="#itempage" onclick="funcsub(' + j + ')" ><div class="ui-grid-d"><div class="ui-block-a"><img src="/Categpics/'+ j +'.png" id="catimg" height=36 class="align-left"/></div><div class="text-align-right">' + getridofquotes(JSON.stringify(each_category['categname'])) + '</div></div></a>';
//		else if(j>9)
//		htmlSource += '<a data-role="button" data-transition="slide" id="ref_for_categ" data-theme="d" href="#itempage" onclick="funcsub(' + j + ')" ><div class="ui-grid-d"><div class="ui-block-a"><img src="/Categpics/'+ j +'.png" id="catimg" height=36 class="align-left"/></div><div class="text-align-right">' + getridofquotes(JSON.stringify(each_category['categname'])) + '</div></div></a>';
		htmlSource += '<div class="grid_6">'
		if(each_category['categname'].search("<br/>") == -1)
		htmlSource += '<a data-role="button"  data-theme="c" href="#" onclick="funcsub(' + j + ')" ><div class="container_12"><div class="grid_3"><img src="/Categpics/'+ j +'.png" id="catimg" height=36 class="align-left" /></div><div class="grid_9" style="line-height:36px;font-size:14px" >' + getridofquotes(JSON.stringify(each_category['categname'])) + '</div></div></a>';
		else
		htmlSource += '<a data-role="button" data-theme="c" href="#" onclick="funcsub(' + j + ')" ><div class="container_12"><div class="grid_3"><img src="/Categpics/'+ j +'.png" id="catimg" height=36 class="align-left" /></div><div class="grid_9" style="line-height:18px;font-size:14px" >' + getridofquotes(JSON.stringify(each_category['categname'])) + '</div></div></a>';
		htmlSource +='</div>'
	}
	htmlSource += '</div>';
	get.innerHTML = htmlSource;
	document.getElementById("ref_for_categ").appendChild(get);
	shopping_categpage();
	jQuery('#categpage').trigger('pagecreate',function(event){alert("Refreshed");});
}

function showSubCats(categid, mysubcats){
	if(mysubcats.length < 3)
		funcMore(0,mysubcats[0]["subcid"]);
	else{
	var htmlString="<div class='container_12'> <div class='grid_1'>&nbsp;</div> <div class='grid_2'><img src='/Categpics/"+(categid) +".png' id='catimg' height=36 /></div><div class='grid_6 text-align-left ui-header' ><h6 class='text-align-text ui-title'>" +  allcateg[categid-1]['categname']+"</h6></div><div class='grid_1'>&nbsp;</div><div class='grid_2'><a href='#' rel='close' data-icon='delete' data-role='button' data-iconpos='notext' data-theme='a' >close</a></div></div><div class='container_12'>";
	for(var i=0;i < mysubcats.length; i++)
		if(mysubcats[i]['subcategname'].search("<br/>") == -1)
		htmlString += "<div class='grid_6' ><a data-role='button'  data-theme='c' href='#' onclick='funcMore("+i+"," + mysubcats[i]["subcid"] + ")'><div class='align-left' style='line-height:28px;font-size:14px' >" + mysubcats[i]["subcategname"] + "</div></a></div>";
		else
		htmlString += "<div class='grid_6' ><a data-role='button'  data-theme='c' href='#' onclick='funcMore("+i+"," + mysubcats[i]["subcid"] + ")'><div class='align-left' style='line-height:14px;font-size:14px' >" + mysubcats[i]["subcategname"] + "</div></a></div>";
	htmlString += "<div class='grid_6'>&nbsp;</div><div class='grid_6'>&nbsp;</div></div>";
  jQuery("#fastclick").simpledialog2({
    mode: 'blank',
//    headerText: '<div class="container_12"> <div class="grid_4"><img src="/Categpics/'+(categid) +'.png" id="catimg" height=36 /></div><div class="grid_8 text-align-left">' +  allcateg[categid-1]['categname']+'</div></div>',
//    headerClose: true,
//   themeHeader: 'd',
	themeDialog:'e',
    blankContent : 
	htmlString 
      // NOTE: the use of rel="close" causes this button to close the dialog.
//      "<a rel='close' data-theme='e' data-role='button' href='#' data-mini='true' data-inline='true' >Close</a>"
    });
     }
	return false;
}
function funcsub(catnumber){		
	//localStorage.clear();
	var found = false;
	globalCatId = catnumber;
	if(!jQuery.isEmptyObject(mysubcats)){
		if(catnumber in mysubcats){
//			$.mobile.changePage("#itempage");
//			navbarlink(mysubcats[catnumber]);
//			func(mysubcats[catnumber][0]["subcid"]);
			showSubCats(catnumber,mysubcats[catnumber]);
			found = true;
		}
	}
	else if("Subcategories" in localStorage){
		mysubcats = JSON.parse(localStorage.getItem("Subcategories"));
		if(catnumber in mysubcats){
//			$.mobile.changePage("#itempage");
//		    navbarlink(mysubcats[catnumber]);
//			func(mysubcats[catnumber][0]["subcid"]);
			showSubCats(catnumber,mysubcats[catnumber]);
			found = true;
		}
	}
	if(!found){
	$.get("/products/choose?cid="+catnumber.toString(),function(data){		//This 'get' method uses the controller to get all data from the server
		if("Subcategories" in localStorage)
			mysubcats = JSON.parse(localStorage.getItem("Subcategories"));
		else{
			if(mysubcats == null)												//If the localStore is empty
				mysubcats = {};
		}
		mysubcats[catnumber] = data;
		localStorage.setItem("Subcategories",JSON.stringify(mysubcats));
//		$.mobile.changePage("#itempage");
//		navbarlink(data);
//		func(data[0]["subcid"]);
		showSubCats(catnumber,data);
	},'JSON');
	}
	return false;
}

function navbarlink(subcatlink){
	var tabdata,getnav;
	var htmlString = "<ul>"
	var htmlStringMore = '<ul data-role="listview" id="moreSubcatList">'
//	$('#getnavitems').empty();											//To delete previous link data of li tag							
	$('#divnavitemholder').empty();
	for(i=0;i<subcatlink.length;i++){
		tabdata=subcatlink[i];
//		getnav = document.createElement("li");
		//getnav.id = "eachnav"+i;
		///getnav.innerHTML = '<a href="#itempage" onclick="func(' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a>';
		if(i==0){
			htmlString += '<li id="firstNavItem"><a class="ui-btn-active ui-state-persist" href="#itempage" onclick="func(' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
			htmlStringMore += '<li><a href="#itempage" onclick="funcMore('+i+',' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
		}
//		else if(i==1)
//			htmlString += '<li><a href="#itempage" onclick="func(' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
		else if(i==1){
			if(subcatlink.length == 2)
				htmlString += '<li><a href="#itempage" onclick="func(' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
			else{
			htmlString += '<li><a href="#"  onclick="showSubCats(' +globalCatId +',' + subcatlink +');"> More... </a></li>';
			htmlStringMore += '<li><a href="#itempage" onclick="funcMore('+i+',' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
			}
		}else
			htmlStringMore += '<li><a href="#itempage" onclick="funcMore('+i +',' + tabdata["subcid"] + ')">' + tabdata["subcategname"] + '</a></li>';
//		document.getElementById("getnavitems").appendChild(getnav);
	}
	htmlString += "</ul>";
	htmlStringMore += "</ul>";
//	alert(htmlStringMore);
	$('#moreSubcatContent').html ( htmlStringMore);
//	$('#moreSubcatList').listview('refresh');
//	alert(htmlString);
//var myNavBar = jQuery('div', {
 //   'data-role':'navbar',
//    'html':htmlString
//}).appendTo("#divnavitemholder").navbar();
	$('#divnavitemholder').html('<div data-role="navbar">' + htmlString + '</div>').trigger('create'); 
//	jQuery('#divnavitem').navbar();
//	jQuery('#getnavitems').listview('refresh');
}

function showMore(){
	
	$.mobile.changePage("#moreSubcatPage", { role: 'dialog'});
	$("#moreSubcatPage").trigger('create');
	$('#moreSubcatList').listview('refresh');
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Backend function
function funcMore(pos,subno){
	var subcateg=mysubcats[globalCatId];
	jQuery(document).trigger('simpledialog', {'method':'close'});
	$('#divnavitemholder').empty();
//	$("#firstNavItem").html('<a class="ui-btn-active ui-state-persist" href="#itempage" onclick="func(' + subcateg[pos]["subcid"] + ')">' + subcateg[pos]["subcategname"] + '</a>');
	var htmlString = '<div data-role="navbar"><ul><li><a class="ui-btn-active ui-state-persist" href="#itempage" onclick="func(' + subcateg[pos]["subcid"] + ')">' + subcateg[pos]["subcategname"] + '</a>';
	if(subcateg.length == 2)
		htmlString += '<li><a href="#itempage" onclick="func(' + subcateg[1]["subcid"] + ')">' + subcateg[1]["subcategname"] + '</a></li>';
	else if(subcateg.length > 2)
		htmlString += '<li><a href="#" onclick="showSubCats(' +globalCatId +',mysubcats[globalCatId]);"> All </a></li>'
	htmlString += '</ul></div>';
//	alert(htmlString);
	$('#divnavitemholder').html(htmlString).trigger('create'); 
//	$('#divnavitemholder').trigger('create'); 
	func(subno);
}
function func(subno){														//Used to list the subcategories
	$('#list1').empty();
	//localStorage.clear(); 													//To clear the local storage if new values are to be added
	var found = false;
//	$.mobile.showPageLoadingMsg("b", "This is only a test", false);
	$('body').addClass('ui-loading')
	getbrandid(subno);
	shopping();	
	$('body').removeClass('ui-loading')
	return;
/*	if(!jQuery.isEmptyObject(mycategories[subno])){										//To print using memory object
		/*if(mycategories[subno]["id"] in purchaseditemid)					//To clear the (RAM) mycategories[subno] when this item is deleted in the 'item' table
			mycategories[subno] = null;* /
		if(subno in mycategories){
			$.mobile.changePage("#itempage");
	//		if(mycategories[subno][0]["cid"] == 1 || mycategories[subno][0]["cid"] == 2 ){
//				alert("its RAM");
	//			print_subcat_list(mycategories[subno]);
	//		}
	//		else
				getbrandid(mycategories[subno][0]["subcid"]);
			found = true;
		}
	}
	else if("Products_Subcateg" in localStorage){						//If memory doesnt have values then intialize and print using memory object
		mycategories = JSON.parse(localStorage.getItem("Products_Subcateg"));		
		if(subno in mycategories){
			$.mobile.changePage("#itempage");
			/*if(mycategories[subno]["id"] in purchaseditemid)
				mycategories[subno] = null;* /
	//		if(mycategories[subno][0]["cid"] == 1 || mycategories[subno][0]["cid"] == 2 ){
//				alert("Its localStore");
	//			print_subcat_list(mycategories[subno]);
	//		}
	//		else
				getbrandid(mycategories[subno][0]["subcid"]);		
			found = true;
		}
	}
	if(!found){															//If both(memory and localstorage) are null then intialize them and print using memory
		$.get("/products/choose?subcid="+(subno).toString(),function(data){		//This 'get' method uses the controller to get all data from the server
			if("Products_Subcateg" in localStorage)
				mycategories = JSON.parse(localStorage.getItem("Products_Subcateg"));
			else{
				if(mycategories == null)
					mycategories = {};
			}
			mycategories[subno] = data;
			localStorage.setItem("Products_Subcateg",JSON.stringify(mycategories));
			$.mobile.changePage("#itempage");
	//		if(mycategories[subno][0]["cid"] == 1 || mycategories[subno][0]["cid"] == 2 )
	//			print_subcat_list(mycategories[subno]);
	//		else
				getbrandid(mycategories[subno][0]["subcid"])
		},'JSON');
	}*/
//	shopping();	
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Printer functions

//prints the list of items
function print_subcat_list(subcateg)				
{
	var listitem,fourcatitems,selectquant,plus,cancel,msg;
	var tabdata;
	listitem = '<ul id="list" data-autodividers="true" data-inset="true" data-filter="true" data-role="listview">';
	fourcatitems = document.createElement("div");
//	alert(subcateg[0]["subcategory"]);	
//	alert(subcateg[0]["subcid"]);	
//	$("#firstNavItem").html('<a class="ui-btn-active ui-state-persist" href="#itempage" onclick="func(' + subcateg[0]["subcid"] + ')">' + subcateg[0]["subcategory"] + '</a>');
//	$('#divnavitemholder').trigger('create'); 
	for (var i=0; i<subcateg.length; i++)					
	{
		tabdata=subcateg[i];							//By specifying the attribute name we can extract each key-value pair from the object; tabdatakey is each attribute in the object and tabdata is the object
		msg='';
		cancel="";
		if((tabdata["subcategory"]) in purchaseditemid){			
			if(tabdata["id"] in purchaseditemid[(tabdata["subcategory"])]){			//This is used to append the cancel button if that item is purchased
				msg = msg_after_add(tabdata["id"],getridofnumbers(tabdata["size"]),purchaseditemid[tabdata["subcategory"]][tabdata["id"]].quantity);
				cancel = msg_after_add_cancel_button(tabdata["id"],(tabdata["subcategory"]),getridofnumbers(tabdata["size"]),purchaseditemid[tabdata["subcategory"]][tabdata["id"]].quantity);
			}	
		}
		if(getridofnumbers(tabdata["size"]) == 'g' || getridofnumbers(tabdata["size"]) == 'ml')				//If the uom is not kg then options need to change
			selectquant='<select name="select-choice-'+tabdata["id"]+'"  id="select-choice-'+tabdata["id"]+'"><option value="0.0">Qty.</option><option value="50.0">50' + getridofnumbers(tabdata["size"]) +'</option><option value="100.0">100' + getridofnumbers(tabdata["size"]) +'</option><option value="250.0">250' + getridofnumbers(tabdata["size"]) +'</option><option value="500.0">500' + getridofnumbers(tabdata["size"]) +'</option></select>'  ;
		else
			selectquant='<select name="select-choice-'+tabdata["id"]+'" id="select-choice-'+tabdata["id"]+'"><option value="0.0">Qty.</option><option value="0.25">1/4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="0.5">1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="0.75">3/4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="1">1 ' + getridofnumbers(tabdata["size"]) + '</option><option value="1.5">1 1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="2">2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="2.5">2 1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="3">3 ' + getridofnumbers(tabdata["size"]) + '</option><option value="4">4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="5">5 ' + getridofnumbers(tabdata["size"]) + '</option> </select>';
		plus='<a href="#itempage" data-role="button"  data-theme="b"  data-icon="plus"  data-iconpos="notext" onclick=\'Addtocart('+tabdata["id"]+',"'+tabdata["subcategory"]+'","'+getridofnumbers(tabdata["size"].toString())+'")\'>plus</a>';
		listitem += "<li><div class='container_16'><div class='grid_8'>"+ nonull(tabdata['variant']) + " " + (tabdata['product'])+"</div><div class='grid_6' >" + selectquant + "</div><div class='grid_2'>"+plus+"</div><div class='grid_8'>"+ "&#8377; " + tabdata['mrp'] + " / " + remove_one(tabdata['size'])  +"</div><div class='grid_2' id='"+ tabdata['id'] +"c'>" +cancel+ "</div> <div class='grid_6' id='"+ tabdata['id'] +"b'>" + msg + "</div></div></li>";
	}
	
	listitem += "</ul>";
	fourcatitems.innerHTML = listitem;
	document.getElementById('list1').appendChild(fourcatitems);
	jQuery('#itempage').trigger( 'pagecreate');
	jQuery('#list').listview('refresh');
}	

function getbrandid(subno){
	$('#list1').empty();														//Removes all its children tags or elements
	var listitem, content_listitem;
	listitem = document.createElement("div");
	content_listitem = '<div data-role="collapsible-set" id="collapset" ><ul data-role="listview" data-theme="d"  data-filter="true" id="outer-ul"></ul></div>';
	$.mobile.changePage("#itempage");
	listitem.innerHTML = content_listitem;
	//Cant give innerHTML in the loop as it will parse that code immedietly which wont allow appending new code
	document.getElementById("list1").appendChild(listitem);
	var found = false;
	if(!jQuery.isEmptyObject(brandofsub)){
		if(subno in brandofsub){
//			alert("Its in brand RAM");
			$.mobile.changePage("#itempage");
			for(var i=0;i<Object.keys(brandofsub[subno]).length-1;i++){
				funcbrand(brandofsub[subno][i]["brandid"], brandofsub[subno][i]["order"],false);
			}
			funcbrand(brandofsub[subno][Object.keys(brandofsub[subno]).length-1]["brandid"], brandofsub[subno][Object.keys(brandofsub[subno]).length-1]["order"],true);
			found = true;
/*	jQuery("#outer-ul").listview("refresh");
	$("#collapset").find('input[data-type="search"]').first().bind('keyup change',function(){
			var val = this.value;	
			if(val == "")
				$("#outer-ul").find(".ui-collapsible").trigger('collapse');
			else
				$("#outer-ul").find(".ui-collapsible").trigger('expand');
			$("#outer-ul").find('input[data-type="search"]').filter(function(){
				$(this).closest('form').hide();
				if($(this).val() != val){
					$(this).val(val);
					$(this).trigger("change");
					return false;
				}
			});
	});*/
//	$("#outer-ul").listview('option', 'filterCallback', function( text, searchValue ){
//		$(".ui-collapsible").trigger('expand');
 //		 return text.toString().toLowerCase().indexOf( searchValue ) === -1;
//	});
		}
	}
	else if("Brand_Subcateg" in localStorage){
		brandofsub = JSON.parse(localStorage.getItem("Brand_Subcateg"));		
		if(subno in brandofsub){
//			alert("Its in brand localStore");
			$.mobile.changePage("#itempage");
			for(var i=0;i<Object.keys(brandofsub[subno]).length-1;i++)	
				funcbrand(brandofsub[subno][i]["brandid"], brandofsub[subno][i]["order"], false);
			funcbrand(brandofsub[subno][Object.keys(brandofsub[subno]).length-1]["brandid"], brandofsub[subno][Object.keys(brandofsub[subno]).length-1]["order"],true);
			found = true;
/*	jQuery("#outer-ul").listview("refresh");
	$("#collapset").find('input[data-type="search"]').first().bind('keyup change',function(){
			var val = this.value;	
//			alert(val);
			if(val == "")
				$("#outer-ul").find(".ui-collapsible").trigger('collapse');
			else
			$("#outer-ul").find(".ui-collapsible").trigger('expand');
			$("#outer-ul").find('input[data-type="search"]').filter(function(){
				$(this).closest('form').hide();
				if($(this).val() != val){
					$(this).val(val);
					$(this).trigger("change");
					return false;
				}
			});
	});*/
		
//	$("#outer-ul").listview('option', 'filterCallback', function( text, searchValue ){
//		if(searchValue == ""){
//			alert("empty");
//			$(".ui-collapsible").trigger('collapse');
//		}else
//				$("#outer-ul").find(".ui-collapsible").trigger('expand');
//		$("#outer-ul").find('input[data-type="search"]').filter(function(){
//			alert(Object.keys($(this)));
//			if($(this).val() != searchValue){
//				$(this).val(searchValue)
//				$(this).trigger("change");
//				return false;
//			}
//		});
 //		 return text.toString().toLowerCase().indexOf( searchValue ) === -1;
//	});
		}		
	}
	if(!found){
		$.get("/products/choose?subcategid="+subno.toString(),function(data){		//This 'get' method uses the controller to get all data from the server
			$.mobile.changePage("#itempage");
			if("Brand_Subcateg" in localStorage)
				brandofsub = JSON.parse(localStorage.getItem("Brand_Subcateg"));
			else{
				if(brandofsub == null)
					brandofsub = {};
			}
			brandofsub[subno] = data;
			localStorage.setItem("Brand_Subcateg",JSON.stringify(brandofsub));

		for(var i=0;i<data.length -1 ;i++){
			brandobj = data[i];
			funcbrand(brandobj["brandid"], brandobj["order"], false );
		}
		brandobj = data[data.length-1];
		funcbrand(brandobj["brandid"], brandobj["order"], true );
	//jQuery(calholder).collapsibleset('refresh');
	//jQuery(idholder).listview('refresh');
//	jQuery('#itempage').trigger( 'pagecreate');
/*	jQuery("#outer-ul").listview("refresh");
	$("#collapset").find('input[data-type="search"]').first().bind('keyup change',function(){
			var val = this.value;	
			if(val == "")
				$("#outer-ul").find(".ui-collapsible").trigger('collapse');
			else
				$("#outer-ul").find(".ui-collapsible").trigger('expand');
			$("#outer-ul").find('input[data-type="search"]').filter(function(){
				$(this).closest('form').hide();
				if($(this).val() != val){
					$(this).val(val);
					$(this).trigger("change");
					return false;
				}
			});
	});*/
//	$("#outer-ul").listview('option', 'filterCallback', function( text, searchValue ){
//		$(".ui-collapsible").trigger('expand');
//		$("outer-ul").find('input[data-type="search"]').val(searchValue);
//		$("outer-ul").find('input[data-type="search"]').trigger("change");
 //		 return text.toString().toLowerCase().indexOf( searchValue ) === -1;
//	});
		},'JSON');
	}
}


function funcbrand(brandno, myorder,islast){	//SORT THE PRODUCTS IN THIS FUNC
	var found = false;
	//localStorage.clear();
	var mykey = "Products_Brand"+brandno.toString();
//	alert(mykey);
	if(!jQuery.isEmptyObject(brandobj)){
		if(brandno in brandobj){
			//$.mobile.changePage("#itempage");
			collapse_for_brands(brandobj[brandno], myorder, islast);
			found = true;
//			alert("Just before display-RAM");
		}
	}
	if(!found && mykey in localStorage){
//		alert(localStorage.getItem("Products_Brand"));
			//$.mobile.changePage("#itempage");
		brandobj[brandno] = JSON.parse(localStorage.getItem(mykey));
//		if(brandno in brandobj){
			collapse_for_brands(brandobj[brandno], myorder,islast);
			found = true;
//			alert("Just before display-Local");
//		}
	}
	if(!found){
		$.get("/products/choose?brandid="+brandno.toString(),function(data){		//This 'get' method uses the controller to get all data from the server
//			if("Products_Brand" in localStorage)
//				brandobj = JSON.parse(localStorage.getItem("Products_Brand"));
//			else{
//				if(brandobj == null)
//					brandobj = {};
//			}
			brandobj[brandno] = data;
//			alert("Its still hitting server");
			localStorage.setItem(mykey,JSON.stringify(data));
			//$.mobile.changePage("#itempage");
			collapse_for_brands(data, myorder,islast);		
		},'JSON');
	}
	return;
}

function collapse_for_brands(branditems, myorder, islast){
	var tabdata,listitem,content_listitem,idholder,calholder,msg,cancel; 
//	listitem = document.createElement("div");
//	content_listitem = '<li><div data-role="collapsible" style="line-height:28px" >';
	content_listitem = '<li><div data-role="collapsible" data-inset="false" >';
	for (var i=0; i < branditems.length; i++){
		tabdata = branditems[i];
		msg="";
		cancel="";
		if(tabdata["subcategory"] in purchaseditemid)
			if(tabdata["id"] in purchaseditemid[tabdata["subcategory"]]){
			msg = msg_after_add_brand(tabdata["id"],getridofnumbers(tabdata["size"]),purchaseditemid[tabdata["subcategory"]][tabdata["id"]].quantity);
			cancel = msg_after_add_cancel_button(tabdata["id"],tabdata["subcategory"],getridofnumbers(tabdata["size"]),purchaseditemid[tabdata["subcategory"]][tabdata["id"]].quantity);
		}
//		plus='<a href="#itempage" class="plusicon" onclick=\'Addtocart2('+tabdata["id"]+',"'+tabdata["subcategory"]+'","'+tabdata["uom"]+'")\'><img width="40" height="20" src="/Addtocart.png"/></a>';
		if(i==0){
			idholder = tabdata["id"] + ":" + i;
			calholder = idholder + "call";
			content_listitem += '<h1 >'+ nonull(tabdata['brand']) +'</h1> <ul id="'+idholder+'" data-role="listview" data-filter="true">';		
		}
	    if(tabdata.uom != null && tabdata.uom != "pc"){
		if(getridofnumbers(tabdata["size"]) == 'g' || getridofnumbers(tabdata["size"]) == 'ml')				//If the uom is not kg then options need to change
			selectquant='<select name="select-choice-'+tabdata["id"]+'"  id="select-choice-'+tabdata["id"]+'"><option value="0.0">Qty.</option><option value="50.0">50' + getridofnumbers(tabdata["size"]) +'</option><option value="100.0">100' + getridofnumbers(tabdata["size"]) +'</option><option value="250.0">250' + getridofnumbers(tabdata["size"]) +'</option><option value="500.0">500' + getridofnumbers(tabdata["size"]) +'</option></select>'  ;
		else
			selectquant='<select name="select-choice-'+tabdata["id"]+'" id="select-choice-'+tabdata["id"]+'"><option value="0.0">Qty.</option><option value="0.25">1/4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="0.5">1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="0.75">3/4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="1">1 ' + getridofnumbers(tabdata["size"]) + '</option><option value="1.5">1 1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="2">2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="2.5">2 1/2 ' + getridofnumbers(tabdata["size"]) + '</option><option value="3">3 ' + getridofnumbers(tabdata["size"]) + '</option><option value="4">4 ' + getridofnumbers(tabdata["size"]) + '</option><option value="5">5 ' + getridofnumbers(tabdata["size"]) + '</option> </select>';
		plus='<a href="#itempage" data-role="button"  data-theme="b"  data-icon="plus"  data-iconpos="notext" onclick=\'Addtocart('+tabdata["id"]+',"'+tabdata["subcategory"]+'","'+getridofnumbers(tabdata["size"].toString())+'")\'>plus</a>';
		content_listitem += "<li><div class='container_16'><div class='grid_8'>"+ nonull(tabdata['variant']) + " " + nonull(tabdata['product'])+"</div><div class='grid_6' >" + selectquant + "</div><div class='grid_2'>"+plus+"</div><div class='grid_8'>"+ "&#8377; " + tabdata['mrp'] + " / " + remove_one(tabdata['size'])  +"</div><div class='grid_2' id='"+ tabdata['id'] +"c'>" +cancel+ "</div> <div class='grid_6' id='"+ tabdata['id'] +"b'>" + msg + "</div></div></li>";
	     }
	     else{
		tabdata.uom = "pc";
		plus='<a href="#itempage" data-role="button"  data-theme="b"  data-icon="plus"  data-iconpos="notext" onclick=\'Addtocart2('+tabdata["id"]+',"'+tabdata["subcategory"]+'","'+getridofnumbers(tabdata["uom"].toString())+'")\'>plus</a>';
//		content_listitem += "<li><div class='container_16'><div class='grid_5'>"+ nonull(tabdata['variant']) +"</div><div class='grid_1 suffix_8'></div><div class='grid_3'>"+plus+"</div></div><div class='container_16'><div class='grid_4'>"+ "&#8377; " + tabdata['mrp'] + "/ " + tabdata['size']  +"</div><div class='grid_9' id='"+ tabdata['id'] +"b'>" +msg+ "</div><div class='grid_3' id='"+ tabdata['id'] +"c'>"+ cancel +"</div></div></li>";
//		content_listitem += "<li><img src='/product.png' height=80/><div class='container_16'><div class='grid_14'><b>"+ nonull(tabdata['variant']) +"</b></div><div class='grid_2'>"+plus+"</div><div class='grid_8'>"+ tabdata['size'] + " | " +"&#8377; " + tabdata['mrp'] +"</div><div class='grid_2' id='"+ tabdata['id'] +"c'>"+ cancel +"</div><div class='grid_6' id='"+ tabdata['id'] +"b'>" +msg+ "</div></div></li>";
		content_listitem += "<li><div class='container_16'><div class='grid_14'><b>"+ nonull(tabdata['variant']) +"</b></div><div class='grid_2'>"+plus+"</div><div class='grid_8'>"+ tabdata['size'] + " | " +"&#8377; " + tabdata['mrp'] +"</div><div class='grid_2' id='"+ tabdata['id'] +"c'>"+ cancel +"</div><div class='grid_6' id='"+ tabdata['id'] +"b'>" +msg+ "</div></div></li>";
	     }
		if(i == (branditems.length-1))
			content_listitem += '</ul></div></div></div></li>';
	}
	jQuery("#outer-ul").append(content_listitem);
	jQuery('#itempage').trigger( 'pagecreate');
	jQuery("#outer-ul").listview("refresh");
	if(islast)
	$("#collapset").find('input[data-type="search"]').first().bind('keyup change',function(){
			var val = this.value;	
			if(val == "")
				$("#outer-ul").find(".ui-collapsible").trigger('collapse');
			else
				$("#outer-ul").find(".ui-collapsible").trigger('expand');
			$("#outer-ul").find('input[data-type="search"]').filter(function(){
//				$(this).closest('form').hide();
				if($(this).val() != val){
					$(this).val(val);
					$(this).trigger("change");
					return false;
				}
			});
	});
	$("#outer-ul").find('input[data-type="search"]').filter(function(){
		$(this).closest('form').hide();
		return false;
	});
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//CART functions

function mapping_items(cartid,countid,totalbillid){								//Creates a hash map of all the items in the cart with the product id as index or key
	
	var allitem = new Object;
	var countobj=0;
	var totalbill=0,tabdata;
	//Checks if there are items in shopping cart
	//alert(cookiecartid);
	if(!jQuery.isEmptyObject(purchaseditemid)){
		for(subcat in purchaseditemid)
			for(item in purchaseditemid[subcat]){
				countobj++;
				tabdata = purchaseditemid[subcat][item]
				if(tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2 )
					totalbill += (parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]))/parseFloat(getridofalphabets(tabdata["item_size"]));
				else
					totalbill += parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]);
			}
		var cartimg = "(" + countobj + ")" + " &#8377;" + (totalbill.toFixed(0));		
//		cartdiv.innerHTML = cartimg; 
		document.getElementById(cartid).innerHTML=(cartimg);

	}
	else{
	$.get("/items/checkcart?cart_id="+cookiecartid.toString(),function(data){		
		$('#'+cartid).empty();
		allitem = data;
		if(data.length != 0)
			for(var i=0;i<allitem.length;i++){
				countobj++;
				tabdata = allitem[i];	
				if(tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2 )
					totalbill += (parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]))/parseFloat(getridofalphabets(tabdata["item_size"]));
				else
					totalbill += parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]);
				if(allitem[i].item_subcateg_name in purchaseditemid)
					purchaseditemid[allitem[i].item_subcateg_name][allitem[i].product_id] = allitem[i];
				else{
					purchaseditemid[allitem[i].item_subcateg_name] = new Object();
					purchaseditemid[allitem[i].item_subcateg_name][allitem[i].product_id] = allitem[i];
				}
			}
//		var cartdiv = document.createElement("div");
//		var cartimg = "<span><a style='text-decoration: none;' onclick='shoppingcart_display()' href='#shoppingcart_page'><img width='27' id='img' height='23' style='margin-top:7px;' src='/shoppingcart22.png'/><span id='"+countid+"'></span><span id='"+totalbillid+"'></span></a></span>";		
		var cartimg = "(" + countobj + ")" + " &#8377;" + (totalbill.toFixed(0));		
//		cartdiv.innerHTML = cartimg; 
		document.getElementById(cartid).innerHTML=(cartimg);
		//alert(purchaseditemid.length);								//Its undefined because its a "hash" object due to which it cant determine its length
		//document.getElementById(countid).innerHTML = '(' + countobj + ')';
		//document.getElementById(totalbillid).innerHTML = '&#8377; ' + (totalbill.toFixed(2));
//		$("#"+cartid).trigger('create');
	},'JSON');
	}
}

function Addtocart(anid,subname,item_uom){						//Non-branded
	var elementid = "select-choice-"+anid;						//Used to get the quantity selected
	var quan = $("#" + elementid).val();						//var txt = document.getElementById(elementid).value; or	var quan = $("#" + elementid).selectedIndex();	 So all these work.
	if(quan == 0.0){
		msg_for_noadd(anid);
		return;
	}
	else{
		newquan = parseFloat(quan);
		if(subname in purchaseditemid && anid in purchaseditemid[subname]) {
			purchaseditemid[subname][anid].quantity = parseFloat(purchaseditemid[subname][anid].quantity);
			quan = purchaseditemid[subname][anid].quantity + newquan;
		}
		msg_for_add_nonbrand(anid,subname,item_uom,quan);
		$.get("/items/select",{ id:anid, quantity:newquan}, function(data){
			if(!(subname in purchaseditemid))
				purchaseditemid[subname] = {};
			if(data != "no")
				purchaseditemid[subname][anid] = data
			shopping();
		});			//The Data in POST is ; <server-side param>:<client-side or local variable>
		return;
	}
}

function Addtocart2(anid,subname,item_uom){						//Branded
	var elementid = "select-choice-"+anid;						//Used to get the quantity selected
	var quan = 1,one = 1;
	if(subname in purchaseditemid && anid in purchaseditemid[subname]) {	
		purchaseditemid[subname][anid]["quantity"] = parseInt(purchaseditemid[subname][anid]["quantity"]);
		quan += purchaseditemid[subname][anid]["quantity"];
	}
	msg_for_add_brand(anid,subname,item_uom,quan);
	$.get("/items/select",{ id:anid, quantity:one}, function(data){
		if(!(subname in purchaseditemid))
			purchaseditemid[subname] = {};
		if(data != "no")
			purchaseditemid[subname][anid] = data
		shopping();
	});			//The Data in POST is ; <server-side param>:<client-side or local variable>
	return;
}

function msg_after_add(anid,item_uom,quan){				//Used in the printer functions-print_subcateg_list
	var msg = quan + item_uom + " in cart";
	return(msg);
}

function msg_after_add_brand(anid,item_uom,quan){				//Used in the printer functions-collapsible-list
	var msg = "In the cart: " + Math.round(quan);
	return(msg);
}

function msg_after_add_cancel_button(anid,subname,item_uom,quan){			//Used in both printer functions
	var cancel='<a href="#itempage" onclick=\'Removefromcart('+anid+','+quan+',"'+item_uom+'","'+ (subname) +'")\'><img width="18" height="18" src="/cancel.png"/></a>';
	return cancel;
}

function msg_for_add_nonbrand(anid,subname,item_uom,quan){				//Used in Addtocart function
	var cancel='<a href="#itempage" onclick=\'Removefromcart('+anid+','+quan+',"'+item_uom+'","'+subname+'")\'><img width="18" height="18" src="/cancel.png"/></a>';
	document.getElementById(anid+'b').innerHTML = quan + item_uom + " in cart";
	document.getElementById(anid+'c').innerHTML = cancel;
	return;
}

function msg_for_add_brand(anid,subname,item_uom,quan){				//Used in Addtocart2 function
	var cancel='<a href="#itempage" onclick=\'Removefromcart('+anid+','+quan+',"'+item_uom+'","'+subname+'")\'><img width="18" height="18" src="/cancel.png"/></a>';
	document.getElementById(anid+'b').innerHTML = "In the cart: " + quan.toFixed(0);
	document.getElementById(anid+'c').innerHTML = cancel;
	return;
}

function msg_for_noadd(anid){							//Used in Addtocart function
	document.getElementById(anid+'b').innerHTML = "No items added";
	return;
}

function Removefromcart(anid,quan,item_uom,subname){			//Used in cancel button in itempage
	if(purchaseditemid[subname][anid]["Category_id"] == 1 || purchaseditemid[subname][anid]["Category_id"] == 2 )
			document.getElementById(anid+'b').innerHTML = quan + item_uom + " removed from cart";
	else
		document.getElementById(anid+'b').innerHTML = quan + " pc(s) removed from cart";
	document.getElementById(anid+'c').innerHTML = "";
	$.get("/items/remove",{ product_id:anid}, function(data){	
	delete purchaseditemid[subname][anid];						//Once the item is deleted it must also be removed from the memory
	if(jQuery.isEmptyObject(purchaseditemid[subname]))
		delete purchaseditemid[subname];
	shopping();
	});
	return;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Shopping functions

function shopping(){
	var cartid = "shoppingcart",countid = "display_cart_no",totalbillid = "display_cart_total";
	mapping_items(cartid,countid,totalbillid);
	return;
}

function shopping_categpage(){
	var cartid = "shoppingcart_categpage",countid = "display_cart_no2",totalbillid = "display_cart_total2";
	mapping_items(cartid,countid,totalbillid);
}
	
function shoppingcart_display(){
	var tabdata;
	var totalbill=0,subtotal=0,cancel;
	var shopping_list = document.createElement("div");
	var shopping_list_holder;
	var subtotal_msg,plus1,plus2,msg1,msg2,selectquant1,selectquant2="&nbsp;";
	var data = purchaseditemid;
	$('#purchaseditemlist').empty();
	$('#purchaseditemlistdiv').empty();						//To refresh the div and the ul
	//alert("hello");
	
	shopping_list.innerHTML = "<h4 align='center' style='font-family:Verdana'>Cart is empty!</h4>";
	
	shopping_list_holder = '<ul data-role="listview" id="purchaseditemlist" data-autodividers="true" data-inset="true" data-filter="true">';
	Totalbill=0;
	for(var subid in data){
		subtabdata = data[subid];		//gets each purchased item
		shopping_list_holder += "<li data-role='list-divider'>" +  subid+ "</li>";
		for(var id in subtabdata){
		tabdata = subtabdata[id];
		plus1='<a href="#" data-role="button"  data-theme="b"  data-icon="plus"  data-iconpos="notext" onclick=\'return Addtocart3('+tabdata["product_id"]+',"'+tabdata["item_subcateg_name"]+'","'+getridofnumbers(tabdata["item_size"].toString())+'")\'>plus</a>';
		plus2 = '<a href="#shoppingcart_page"  data-role="button"  data-theme="b"  data-icon="plus"  data-iconpos="notext"  onclick=\'Addtocart3('+tabdata["product_id"]+',"'+tabdata["item_subcateg_name"]+'","'+tabdata["item_uom"]+'")\'><img width="44" height="22" src="/Addtocart.png"/>plus</a>';
		if((tabdata["item_subcateg_name"]) in purchaseditemid){			
			if(tabdata["product_id"] in purchaseditemid[(tabdata["item_subcateg_name"])]){			//This is used to append the cancel button if that item is purchased
				if(tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2)
					msg1 = msg_after_add(tabdata["product_id"],getridofnumbers(tabdata["item_size"]),purchaseditemid[tabdata["item_subcateg_name"]][tabdata["product_id"]].quantity);
				else
					msg2 = msg_after_add_brand(tabdata["product_id"],getridofnumbers(tabdata["item_size"]),purchaseditemid[tabdata["item_subcateg_name"]][tabdata["product_id"]].quantity);
			}	
		}
		if(getridofnumbers(tabdata["item_size"]) == 'g' || getridofnumbers(tabdata["item_size"]) == 'ml')				//If the uom is not kg then options need to change
			selectquant1='<select name="select-choice2-'+tabdata["product_id"]+'"  id="select-choice2-'+tabdata["product_id"]+'"><option value="0.0">Qty.</option><option value="50.0">50' + getridofnumbers(tabdata["item_size"]) +'</option><option value="100.0">100' + getridofnumbers(tabdata["item_size"]) +'</option><option value="250.0">250' + getridofnumbers(tabdata["item_size"]) +'</option><option value="500.0">500' + getridofnumbers(tabdata["item_size"]) +'</option></select>'  ;
		else
			selectquant1='<select name="select-choice2-'+tabdata["product_id"]+'" id="select-choice2-'+tabdata["product_id"]+'"><option value="0.0">Qty.</option><option value="0.25">1/4 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="0.5">1/2 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="0.75">3/4 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="1">1 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="1.5">1 1/2 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="2">2 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="2.5">2 1/2 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="3">3 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="4">4 ' + getridofnumbers(tabdata["item_size"]) + '</option><option value="5">5 ' + getridofnumbers(tabdata["item_size"]) + '</option> </select>';
		cancel = msg_after_add_3(tabdata["product_id"],tabdata["item_subcateg_name"],tabdata["item_uom"],tabdata["quantity"]);
		subtotal = ((tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2 )?(parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]))/parseFloat(getridofalphabets(tabdata["item_size"])):parseFloat(tabdata["quantity"])*parseFloat(tabdata["mrp"]));
		subtotal_msg = "  " + tabdata["quantity"] + " " + ((tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2)?getridofnumbers(tabdata["item_size"]):"pc(s)") + ", &#8377; " + parseFloat(subtotal.toFixed(2));
		if((tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2))
			shopping_list_holder += "<li><div class='container_16'><div class='grid_8'>"+ nonull(tabdata['product_name'])+" "+nonull(tabdata['variant_name']) +" " +nonull(tabdata['brand_name'])+"</div><div class='grid_6' >"+ selectquant1 + "</div><div class='grid_2'>" + plus1 +"</div><div class='grid_8'>"+ "&#8377; " + tabdata['mrp'] + "/" + remove_one(tabdata['item_size'])  + "</div><div class='grid_2' id='"+ tabdata['product_id'] +"sc'>" + cancel + "</div><div class='grid_6' id='"+ tabdata['product_id'] +"sb'>"+ ((tabdata["Category_id"] == 1 || tabdata["Category_id"] == 2)?( subtotal_msg  ):(subtotal_msg)) +"</div></div></li>";
		else
			shopping_list_holder += "<li><div class='container_16'><div class='grid_14'>"+ nonull(tabdata['brand_name']) + " " + nonull(tabdata['product_name'])+" " +nonull(tabdata['variant_name'])+"</div><div class='grid_2'>"+ (plus2) +"</div><div class='grid_8'>"+ tabdata['item_size'] + " | &#8377; " + tabdata['mrp']  + "</div><div class='grid_2' id='"+ tabdata['product_id'] +"sc'>" + cancel + "</div><div class='grid_6' id='"+ tabdata['product_id'] +"sb'>"+ subtotal_msg  +"</div></div></li>";
		Totalbill += subtotal;
		}
	}	
	shopping_list_holder += "</ul>";
	shopping_list_holder += "<br/><h3 id='h3' align='right' style='background-color:white; border: solid 5px #99aabb;'>Total :&#8377; " + (parseFloat(Totalbill).toFixed(2)) + "</h3>";
	//shopping_list_holder += "<a data-role='button' href='#' data-icon='check' data-iconpos='right'>Checkout</a>";
	shopping_list_holder += "<a data-role='button' href='#checkout_page' onclick='checkout("+ (parseFloat(Totalbill).toFixed(2)) +")' data-icon='check' data-iconpos='right' data-theme='e'>Checkout</a>";
	if(Totalbill != 0)
		shopping_list.innerHTML = shopping_list_holder;
	document.getElementById("purchaseditemlistdiv").appendChild(shopping_list);
	//jQuery('#purchaseditemlist').listview('refresh');			
	jQuery('#shoppingcart_page').trigger('pagecreate');
}

function remove_one(size){
	var regex = new RegExp("1 ", "g");
	return(size.replace(regex, ""));
}
function Addtocart3(anid,subname,item_uom){						//Non-branded
	var elementid = "select-choice2-"+anid;						//Used to get the quantity selected
	var quan,newquan,one;
	if(subname == "Vegetables" || subname == "Fruits" || subname == "Dals & Pulses"){
		quan = $("#" + elementid).val();
		if(quan == 0.0)
			msg_for_noadd_cart(anid);
		else{
			newquan = parseFloat(quan);
			if(subname in purchaseditemid && anid in purchaseditemid[subname]) {
				purchaseditemid[subname][anid].quantity = parseFloat(purchaseditemid[subname][anid].quantity);
				quan = purchaseditemid[subname][anid].quantity + newquan;
			}
			$.get("/items/select",{ id:anid, quantity:newquan },function(data){purchaseditemid[subname][anid].quantity = quan;shoppingcart_display();});			//The Data in GET or POST is ; <server-side param>:<client-side or local variable>
			return false;
		}
	}
	else{
		quan = 1,one = 1;
		if(subname in purchaseditemid && anid in purchaseditemid[subname]) {	
			purchaseditemid[subname][anid]["quantity"] = parseInt(purchaseditemid[subname][anid]["quantity"]);
			quan += purchaseditemid[subname][anid]["quantity"];
		}
		$.get("/items/select",{ id:anid, quantity:one},function(data){
			if(!(subname in purchaseditemid)){
				purchaseditemid[subname] = {}
			}
			if(data!="no")
				purchaseditemid[subname][anid]=data;
			shoppingcart_display();
		});			//The Data in GET or POST is ; <server-side param>:<client-side or local variable>
		return false;
	}
}

function msg_after_add_3(anid,subname,item_uom,quan){			//This is used in shopping cart
	var cancel='<a href="#shoppingcart_page" onclick=\'Removefromcart2('+anid+','+quan+',"'+item_uom+'","'+subname+'")\'><img width="18" height="18" src="/cancel.png"/></a>';
	return cancel;
}

function msg_for_noadd_cart(anid){
	document.getElementById(anid+'sb').innerHTML = "No items added";
	return;
}

function Removefromcart2(anid,quan,item_uom,subname){			//This is used in shoppingcartpage
	if(purchaseditemid[subname][anid]["Category_id"] == 1 || purchaseditemid[subname][anid]["Category_id"] == 2 ){
		Totalbill -= (parseFloat(purchaseditemid[subname][anid]["quantity"])*parseFloat(purchaseditemid[subname][anid]["mrp"]))/parseFloat(getridofalphabets(purchaseditemid[subname][anid]["item_size"]));
		document.getElementById(anid+'sb').innerHTML = quan + item_uom + " removed from cart";
	}
	else{
		Totalbill -= (parseFloat(purchaseditemid[subname][anid]["quantity"])*parseFloat(purchaseditemid[subname][anid]["mrp"]));
		document.getElementById(anid+'sb').innerHTML = quan + "pc(s) removed from cart";
	}
	document.getElementById(anid+'sc').innerHTML = "";
	$.get("/items/remove",{ product_id:anid});	
	document.getElementById("h3").innerHTML = "Total :&#8377; " + (Totalbill.toFixed(2));
	delete purchaseditemid[subname][anid];
	if(jQuery.isEmptyObject(purchaseditemid[subname]))
		delete purchaseditemid[subname];
	shopping();
	return;
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Checkout functions

function checkout(total1){
	$('#confirmorderdiv').empty();
	$('#confirmorderlistdiv1').empty();	
	$('#confirmorderlistdiv2').empty();	
	var deliverytime = [{hour:9,min:30},{hour:15,min:30},{hour:19,min:0}]
	var timenow = new Date();
	var deliveryDate = new Date();
	deliveryDate.setSeconds(0);
	var numdeloption = 0;
	var checkoutinfo,checkoutinfo_content;
	
	$.get("/shoppingcarts/checkout",{cart_id:cookiecartid, total:total1},function(data){		//data will have address
		checkoutinfo = document.createElement("div");
		checkoutinfo_content = "<h5>Confirm Order</h5><ul data-role='listview' id='confirmorderlistdiv1'><li data-role='list-divider'>Payment Info</li><li>Order Amount: &#8377; "+ total1 +"</li><li>Cash on Demand (COD)</li>";
		checkoutinfo_content += "<li data-role='list-divider'>Delivery time</li><li>";
		checkoutinfo_content += '<fieldset data-role="controlgroup">'
		while(numdeloption < 4){
			for(var i=0;i<deliverytime.length;i++){
				deliveryDate.setHours(deliverytime[i].hour);
				deliveryDate.setMinutes(deliverytime[i].min);
				if(deliveryDate > timenow){
					if(numdeloption == 0)
					checkoutinfo_content += '<input type="radio" name="radio-choice" id="radio-choice-' +numdeloption + '" checked="checked" />'
					else
					checkoutinfo_content += '<input type="radio" name="radio-choice" id="radio-choice-' +numdeloption + '" />'
     					checkoutinfo_content += '<label for="radio-choice-'+ numdeloption +'">'+deliveryDate.toLocaleString().replace("GMT+05:30","")+'</label>'
					numdeloption ++;
					if(numdeloption == 4)
						break;
				}
			}
			deliveryDate.setDate(deliveryDate.getDate()+1);
		}
		checkoutinfo_content += "</fieldset> </li><li data-role='list-divider'>Delivery Address</li></ul><br/><br/><textarea id='firsttextarea'>" + data  + "</textarea><br/>";
		checkoutinfo_content += "<a data-role='button' data-theme='e' href='#placeorder_page' onclick=placeorder(document.getElementById('firsttextarea').value) data-icon='check' data-iconpos='right'>Place order</a>";
		checkoutinfo.innerHTML = checkoutinfo_content;
		document.getElementById("confirmorderdiv").appendChild(checkoutinfo);
		shopping_checkoutpage();
		jQuery('#checkout_page').trigger( 'pagecreate');
		jQuery('#confirmorderlistdiv1').listview('refresh');
		jQuery('#confirmorderlistdiv2').listview('refresh');
	});
}

function shopping_checkoutpage(){
	var cartid = "shoppingcart_checkout",countid = "display_cart_no3",totalbillid = "display_cart_total3";
	mapping_items(cartid,countid,totalbillid);
	return;
}

function placeorder(newaddr){
	$('#myorderdiv').empty();
	$('#placeorderlistdiv').empty();
	var placeorder,placeorder_content;
	
	$.get("/shoppingcarts/placeorder",{ cart_id:cookiecartid, ship_addr:newaddr},function(data){	//data will have orderno.
		placeorder = document.createElement("div");
		placeorder_content = "<ul data-role='listview' id='placeorderlistdiv1' data-divider-theme='d' data-theme='e'><li data-role='list-divider'>My pending orders</li><li>Pending Order #"+ cookiecartid +": &#8377; "+ (data[0]['final_price']) +"</li><li>Estimated Delivery: Based on previous page selection</li><li>Order placed: "+ datemod(data[0]['updated_at']) +"</li><br/>";
		placeorder_content += "<ul data-role='listview' data-divider-theme='d' data-theme='c' id='placeorderlistdiv2'><li data-role='list-divider'>My past order(s)</li><li>Use loop to get print all cart amounts from controller</li></ul><br/>";
		placeorder.innerHTML = placeorder_content;
		document.getElementById("myorderdiv").appendChild(placeorder);
		jQuery("#placeorder_page").trigger("pagecreate");
		jQuery("#placeorderlistdiv1").listview("refresh");
		jQuery("#placeorderlistdiv2").listview("refresh");
	},'JSON');
}



//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Login functions



//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Misc functions

function datemod(update)
{
	var formatdate,formattime,format; 
	update = new Date(update);
	formatdate = update.toLocaleDateString();
	formattime = update.toLocaleTimeString();
	format = formatdate + ", " + formattime;
	return format;
}
function getridofquotes(str){
	var x = str.replace(/\"/g,""); 
	x = x.replace(/null/g,"-");
	return x;
}

function nonull(str){
	if(str == null)									//Cant use replace() because we are passing objects not string so this way suitable
		return ""
	else
		return str;
}

function getridofalphabets(str){
	var x = str.replace(/[a-zA-Z]/g,"");
	x = x.replace(/ /g,"");
	return x;
}

function getridofnumbers(str){
	var x = str.replace(/\d+/g,"");
	x = x.replace(/:/g,"");
	x = x.replace(/ /g,"");
	return x;
}
