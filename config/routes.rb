Schema::Application.routes.draw do
	match "login" => "shoppers#login", :as => :login
	match "out" => "shoppers#logout", :as => :out
	match "m" => "products#choose#categpage", :as => :m
	
  resources :retailers do
  	member do
  		get 'orders'
  	end
  	collection do
  		get 'delivery_times'
      post 'order_sent'
      post 'order_delivered'
      post 'undo_order_delivery'
  	end
  end

  resources :shoppers do
		member do
			get 'orders'
			get 'copy'
			post 'login_check_ajax'
			
		end
		collection do
				post 'register'
				get 'mobile_register'
				get 'mobile_verify'
				get 'mobile_remind_password'
				get 'getorders'
				get 'login'
				post 'login_check'
				post 'login_check_ajax'
				get 'logout'
    end
	end
  resources :items do
		collection do
				get 'select'
				get 'remove'
				get 'checkcart'
				get 'favorites'
        end
	end
  resources :shoppingcarts do
		collection do
				get 'checkout'
				get 'placeorder'
				get 'getorders'
				post 'processOrder'
				get 'get_cart_details'
    end
	end

  resources :brands

  resources :subcategs

  resources :categs 

  resources :products do
  collection do
				get 'choose'			
        end
	end
	
	resources :admin do
	collection do
		get 'compute_subcategbrand_promotions'
		get 'autocompute_product_promotions'
	end
	end
	
	resources :my_lists do
  collection do
				get 'create_my_list'
				get 'all_my_lists'
				get 'get_by_id'
        end
	end
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
