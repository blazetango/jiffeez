set :application, "jiffeez1"
set :repository,  "git@bitbucket.org:blazetango/jiffeez.git"
set :scm, :git 
set :user, "ec2-user" 
set :use_sudo, false
set :deploy_to, "/var/www/#{application}"
set :scm_passphrase, "123123"

set :rake,"rake"
set :rails_env, "development"
set :migrate_env,"development"
set :migrate_target, :latest

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:auth_methods] = "publickey"
ssh_options[:keys] = "/home/chetan/jiffeez/jzsgtest2.pem"
# You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "ec2-54-251-218-190.ap-southeast-1.compute.amazonaws.com"                          # Your HTTP server, Apache/etc
role :app, "ec2-54-251-218-190.ap-southeast-1.compute.amazonaws.com"                          # This may be the same as your `Web` server
role :db,  "ec2-54-251-218-190.ap-southeast-1.compute.amazonaws.com", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  task :run_migration do
    run "cd #{release_path} && rake db:migrate RAILS_ENV=development" #--deployment"
  end   
  task :bundle_install do
    run "cd #{release_path} && bundle install" #--deployment"
  end
  task :start_worker do
    run "cd #{release_path} && ruby script/delayed_job restart RAILS_ENV=development"
  end
  task :start_clock do
    run "cd #{release_path} && clockworkd -c lib/scheduler.rb restart RAILS_ENV=development"
  end
end

after "deploy:update_code", "deploy:bundle_install"
after "deploy:bundle_install", "deploy:run_migration"
after "deploy:run_migration", "deploy:start_worker"
after "deploy:start_worker", "deploy:start_clock"
