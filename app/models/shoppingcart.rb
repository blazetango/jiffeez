require 'simple_record'
class Shoppingcart < SimpleRecord::Base
  set_domain_name :shoppingcart
  has_floats   "final_price", "merchant_price"
  has_dates     "duration"
  has_floats    "discount", "mrp_price"
  has_strings   "state", "delivery_address", "delivery_time", "short_id", "retailer_note"
  has_ints  "items_count"
  has_booleans :is_sent
#added by mandar
	#def short_id
	#	if (self.unique_id.nil?)
	#		self.id[0..2]
	#	else
	#		self.unique_id
	#end
	
	before_save :init_short_id

	def init_short_id
		#puts "after_initialize #{self.short_id}"
    if (self.short_id.nil?)
			self.short_id = Time.now.to_i.to_s[-6..-1]
			#puts "after_initialize:: #{self.short_id}"
		end
  end
	
	def get_time_range
		
		@time_from = DateTime.parse(self.delivery_time).to_time
		@time_to	 = @time_from + 1.hours
		
		@time_range = @time_from.strftime("%a %d %b, %Y between %I:%M %p - ") + @time_to.strftime("%I:%M %p")
		return @time_range
		
	end
#	def as_json(options = { })
		
		#exclude unique_id to avoid confusion with short_id
		#short_id is an alias for unique_id for backward compatibilit 
		#options[:except] ||= :unique_id 
#    props = super(options)
#		puts ("The id: #{self.id}")
#   props[:short_id] = short_id
#    props
#	end
#    has_strings "shopper_id", "retailer_id"
	belongs_to :retailer
	belongs_to :shopper
# changes done by mandar 
  def self.mark_order_as_sent(cart_id,retailer_id,flag)
    begin
      @cart = self.select(:first,:conditions => ["id =? AND retailer_id = ?",cart_id,retailer_id])
      @cart.is_sent = flag
      @cart.save
      return true
    rescue => e
      return false
    end
  end
  def self.mark_order_as_delivered(cart_id,retailer_id)
    begin
      @cart = self.select(:first,:conditions => ["id =? AND retailer_id = ?",cart_id,retailer_id])
      @cart.state = "completed"
      @cart.save
      return true
    rescue => e
      return false
    end
  end

  def self.undo_order_delivery(cart_id,retailer_id)
    begin
      @cart = self.select(:first,:conditions => ["id =? AND retailer_id = ?",cart_id,retailer_id])
      @cart.state = "in-process"
      p @cart.save
      return true
    rescue => e
      return false
    end
  end
end
