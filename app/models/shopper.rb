require 'simple_record'
class Shopper < SimpleRecord::Base
  
	set_domain_name :shopper
	
  has_strings			"name",   
									"mobile",
									"email",
									"address", 
									"userid",   
									"password", 
									"location",  
									"role", 
									"status", 
									"auth_token", 
									"auth_token_timestamp"
		
	has_ints				"auth_token_retry_count"
	
	def is_retailer
			role.include?("retailer")
	end
	
	def is_admin
			role.include?("admin")
	end
	
	has_strings :retailer_id
#	belongs_to :Retailer

	# if user id matches pattern of 10 digits its a mobile number
	def is_userid_mobile
		return !((self.userid =~ /\A\d{10}\z/).nil?)
	end

end
