require 'simple_record'
class Favorites < SimpleRecord::Base
	set_domain_name :favorites
	has_strings "favorite_items","product_id" # a json list of [{item_name, prod_id, ranking}]}
	has_floats "rank"

	belongs_to :shopper
#added by mandar
	def self.update_rank(current_cart)
		cart_id = current_cart.id
		@items = Item.find(:all, :conditions => ["cart_id = ?", cart_id]).as_json;
		@items.each do |item|
			json_item = item.as_json
			@favourite_item = Favorites.find_by_product_id(json_item["product_id"])
			if @favourite_item.blank?
				@new_fav_item = Favorites.new
				@new_fav_item.product_id = json_item["product_id"]
				@new_fav_item.rank = 1
				@new_fav_item.shopper_id = current_cart.shopper_id
				@new_fav_item.save
			else
				@favourite_item.rank =  (@favourite_item.rank.to_i + 1)
				@favourite_item.shopper_id = current_cart.shopper_id
				@favourite_item.save
			end
		end
	end
#added by mandar

	def self.decay_rank
		@all_favorites = Favorites.all
		@all_favorites.each do |favorite|
			begin
				rank = favorite.rank 
				favorite.rank = (rank.to_f * ENV['DECAY_CONSTANT'].to_f).to_s
				favorite.save
			rescue => e 
				puts e 
			end
		end
	end	

end


# Favourite = JSON.encode (object)
# Favourite[1] = JSON.encode(product[1])
# {shopper_id, [{product_name, prod_id}]}
