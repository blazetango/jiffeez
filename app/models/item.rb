require 'simple_record'
class Item < SimpleRecord::Base
   set_domain_name :item
    has_ints  "Category_id"
    has_strings   "quantity", 
									"state",  
									"product_name",  
									"brand_name",  
									"variant_name",  
									"item_uom",  
									"item_size",  
									"item_subcateg_name", 
									"anid"					# the anid which could be null if the item was 
																	# not a part of a group
    has_floats    "mrp",  "merchant_price", "discount", "final_price"
    has_strings "cart_id"
    has_booleans "is_retailer_removed"
	belongs_to :product
#	belongs_to :shoppingcart
end
  
 