# To change this template, choose Tools | Templates
# and open the template in the editor.

require 'simple_record'
class MyList < SimpleRecord::Base
  
	set_domain_name :my_list

	has_strings "list_name",				# => The list name
							"list_items"				#	a json list of [ {product_id, quantity }]}
	
	belongs_to :shopper
	
end