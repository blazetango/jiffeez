class Product < ActiveRecord::Base
	belongs_to :Categ
	belongs_to :Subcateg
	belongs_to :Brand
	
	def self.lookup_anid(product_id)
		
		@product = Product.find(:first, :conditions => ["uom like ?", "%:#{product_id},%"])
		@anid = product_id

		if (!@product.nil?)
			@anid = @product.id
		end
		logger.debug("product anid: #{@anid} for input #{product_id}")

		return @anid
		
	end
	
	def auto_compute_promotion	
#added by mandar	
		@PromoDiscountLimit = 5.0
		@uom_string = self.uom.to_s
		@uom_string_check = self.valid_json?(@uom_string)
		if @uom_string_check
			@uom_json = JSON.parse(@uom_string)
			if @uom_json.count > 1
				@uom_json.each do |item|
					item["mrp"].to_f
					item["price"].to_f
					@discount_percent = ((item["mrp"].to_f - item["price"].to_f) * 100 / item["mrp"].to_f ) 
					is_promotional_item = (@discount_percent > @PromoDiscountLimit)? 1 : 0
					if is_promotional_item == 1
						self.is_promotion = 1
						break
					else
						self.is_promotion = 0
					end
				end
				self.save
			else
				calculate_promo_flag(self,@PromoDiscountLimit)
			end
		else
			calculate_promo_flag(self,@PromoDiscountLimit)
		end
	end
	
	def self.auto_compute_promotions
		@products = Product.all
		@products.each do |prod|
			prod.auto_compute_promotion
			prod.save
		end
#added by mandar
	end
	
	def valid_json? json_  
	  JSON.parse(json_)  
	  return true  
		rescue JSON::ParserError  
	  	return false  
	end

	def calculate_promo_flag(item,promo_discount_limit)
		discount_percent = ((item.mrp.to_f - item.sellprice.to_f) /item.mrp.to_f) * 100
		item.is_promotion = (discount_percent > promo_discount_limit)? 1 : 0
		item.save
	end

end
