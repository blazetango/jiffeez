require 'simple_record'
class Retailer < SimpleRecord::Base
   set_domain_name :retailer
    has_strings   "name",  "address",  "deliveryTime", "delivery_delay"
    has_ints  "weeklyException"
end
#added by mandar
