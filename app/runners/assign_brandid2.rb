class Assign_brandid2 < ActiveRecord::Base
  @product = Product.all
  uniqbid = 0 
  catsubcat = Hash.new
  catsubcatrank = Hash.new
  catsubcatcount = Hash.new
  @product.each do |prod|  
  if(!prod.cid.nil? && !prod.subcid.nil?)
	keys = sprintf("%d:%d:%s",prod.cid,prod.subcid,prod.brand)
	if catsubcat.has_key?(keys)			#if product has the same cid and subcid and brand name then it must get that brand id
		catsubcatrank[keys] += prod.rank
		catsubcatcount[keys] = catsubcatcount[keys] + 1
		prod.brandid = catsubcat[keys]
		prod.save
	else								#Now the product is new is it doesnt have a brandid so it is given and also updated for that key
		uniqbid+=1
		order = prod.brandid;
		catsubcat[keys] = uniqbid
		catsubcatrank[keys] = prod.rank
		catsubcatcount[keys] = 1
		prod.brandid = uniqbid
		prod.save
		branddata = Brand.new
		branddata.id = uniqbid
		branddata.brandid = prod.brandid
		branddata.subcategid = prod.subcid
		branddata.cid = prod.cid
		branddata.brandname = prod.brand
		if(order != 1)
			order = 99
		end
		branddata.order = order
		branddata.save
	end
   end
  end
  @brands = Brand.all
  @brands.each do |brand|
	keys = sprintf("%d:%d:%s",brand.cid, brand.subcategid,brand.brandname)
	#if(catsubcatcount[keys] > 2)
	#brand.order =  catsubcatrank[keys]/catsubcatcount[keys]
	#else
	#brand.order  = 1
	#end
	#make sure its 0 padded as its a string
	if(catsubcatcount[keys] < 10)
		brand.order =  sprintf("00%d",catsubcatcount[keys])
	elsif(catsubcatcount[keys] < 100)
		brand.order =  sprintf("0%d",catsubcatcount[keys])
	else
		brand.order =  catsubcatcount[keys];
	end

	brand.save
  end
end
