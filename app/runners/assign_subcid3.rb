class Assign_subcid3 < ActiveRecord::Base
  @product = Product.all
  uniqsid = 0 
  cat = Hash.new
  @product.each do |prod|
   if(!prod.cid.nil?)
	keys = sprintf("%d:%s",prod.cid,prod.subcategory)
	if cat.has_key?(keys)
		prod.subcid = cat[keys]
		prod.save
	else
		uniqsid+=1
		cat[keys] = uniqsid
		prod.subcid = uniqsid
		prod.save
	end
   end
  end
end