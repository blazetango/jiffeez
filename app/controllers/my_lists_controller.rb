class MyListsController < ApplicationController
	
	def create_my_list
		@new_list = MyList.new
		
		@new_list.list_name = params[:list_name]
		@new_list.shopper_id = session[:id]
		
		@cart_items = Item.find(:all, :conditions => ["cart_id = ? ", session[:cart_id]]);
		
		@my_list_items = Array.new
		
		@cart_items.each do |item|

			@item = {
				:product_id => item.product_id,
				:quantity => item.quantity
			}

			@my_list_items << @item

		end
		@new_list.list_items = ActiveSupport::JSON.encode(@my_list_items)
		
		@new_list.save
		
		render :json =>  { :status => 'ok', :new_list_id => @new_list.id }
		
	end
	
	


	def all_my_lists
    @shopper_id = session[:id]
    @my_lists_db  = MyList.find(:all, :conditions => ["shopper_id = ?", @shopper_id]);
    @my_lists = Array.new
   	@my_lists_db.each  do  |my_list_rec|
	    @my_list = { :list_name => my_list_rec.list_name, :list_id => my_list_rec.id } 
  	  logger.debug('list id: ' + my_list_rec.id)
    	@my_lists << @my_list
     end	
  	@my_lists_json = ActiveSupport::JSON.encode(@my_lists)
    render :json => { :status => 'ok', :my_lists => @my_lists_json }
  end

	def get_by_id
		
		@list_id = params[:list_id]
		@my_list =  MyList.find_by_id(@list_id)
		
		@list_items = ActiveSupport::JSON.decode(@my_list.list_items)
	  @product_id_list = @list_items.map {|x| x['product_id']}
		 
		@product_list = Product.find(:all, :conditions => ["id in (?)", @product_id_list])
		
		# converts the products into item objects
		@my_list_items = Array.new
		
		@product_list.each do |my_prod|
			
			@item = Hash.new
			
			@item[:product_id] = my_prod.id
			@item[:item_subcateg_name] = my_prod.subcategory
			@item[:product_name] = my_prod.product
			@item[:brand_name] = my_prod.brand
			@item[:variant_name] = my_prod.variant
			@item[:item_uom] = (my_prod.uom.nil? || my_prod.uom.index('{') != nil)? "pc" : my_prod.uom
			@item[:item_size] = my_prod.size
			@item[:mrp] = my_prod.mrp
			@item[:merchant_price] = my_prod.sellprice
			@item[:Category_id] = my_prod.cid	
			@item[:anid] = Product.lookup_anid(my_prod.id)
			@item[:quantity] = @list_items.select {|p| p['product_id'] == my_prod.id.to_s}[0]['quantity']
			
			@my_list_items << @item
		end
		
  	render :json => { :status => 'ok', 
			:my_list => {
				
					:list_name => @my_list.list_name,
					:shopper_id => @my_list.shopper_id,
					:list_items => @my_list_items 
			}
		}
		
	end
	
	def remove
		
		@list_id = params[:list_id]
    @my_list =  MyList.find_by_id(@list_id) 

		@my_list.destroy
		
		render :json => { :status => 'ok' }
	end
	
end
