class ApplicationController < ActionController::Base
	
	USERS = { "Admin" => "admin", "Superadmin" => "admin123" }
  
	protect_from_forgery
  
	#TODO: development mode only - remove this later on
	skip_before_filter :verify_authenticity_token
  before_filter :update_session_expiration_date
	#before_filter :authenticate, :only => [:index, :edit, :new]
	
	private
  def update_session_expiration_date
		
		logger.debug "URL: #{controller_name} -- #{action_name} User: --> #{session[:id]} Role: #{session[:user_role]}"
		
		headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'
		
		logger.debug "Test User: #{ params[:test_user] }"
		
		if(!params[:test_user].blank?)
			testuser_authenticate
			return
		end
		
		@login_actions = %w[login create new login_check logout mobile_register mobile_verify mobile_remind_password]
		
		if (@login_actions.include?(action_name))
			return
		end
		
		if(session[:id].blank?)
			if(!cookies.signed[:token].nil?)
				session[:id] = cookies.signed[:token][0]
				session[:cart_id] = cookies.signed[:token][1]
				
				if (!session[:id].blank? )
					@shopper = Shopper.find_by_id(session[:id])
					if (@shopper.nil?)
						redirect_to "/shoppers/login",:alert => "Your current session has expired. Please log out and login again."
						return
					end
					session[:user_role] = @shopper.role
				end
			end
		end
		
#		if (session[:user].nil?  && action_name != "login")
#			redirect_to "/shoppers/login",:alert => "Login required for access"
#			return
#		end
		
		@admin_actions = %w[index edit new copy]
		
		if (action_name != "login" && action_name != "login_check" && action_name != "logout")
			
			logger.debug "ROLE: #{session[:user_role]}"
			if (session[:user_role].nil?)
				#render :js => "Your current session has expired. Please log out and login again."
				logout "Access required for this action. Please sign in to continue.", false
				return
			end
			
			logger.debug "Checking for [#{@admin_actions}] with: #{action_name}"
			if (@admin_actions.include?(action_name) && !session[:user_role].include?("admin"))
				#TODO: make this an alert or something like that
				logger.debug "Redirecting..."
				logout "Access required for this action. Please sign in to continue.", false
				return
			end
			
			if (controller_name == "retailers" && action_name == "orders" && !session[:user_role].include?("retailer"))
				logout "Access required for this action. Please sign in to continue.", false
				return
			end
			
			# allow only shoppers to products - choose
			if (controller_name == "products" && action_name == "choose" && !session[:user_role].include?("shopper"))
				logout "Access required for this action. Please sign in to continue.", false
				return
			end
			
			# allow only shoppers to shoppers - favorites
			if (controller_name == "shoppers" && action_name == "get_favorite_products" && !session[:user_role].include?("shopper"))
				logout "Access required for this action. Please sign in to continue.", false
				return
			end
			logger.debug "No redirection"
		end
		
  end
	
	private
	def authenticate
		
    authenticate_or_request_with_http_digest do |username|
      USERS[username]
    end
  end
	
	def logout(msg, show_alert=true)
		@js = ""
		@js = "alert('#{msg}');" if (show_alert)
		clear_session
		#redirect_to "/shoppers/login"
		render :inline => "<script type='text/javascript'>#{@js}localStorage.clear();window.location.href='/login'</script>"
	end
	
	def clear_session
		session["id"]=""
		session["cart_id"] = ""
		session["user_role"] = ""

		cookies.permanent.signed[:token] = ["",""];
	end


	def testuser_authenticate
		
			@userid = params[:userid].strip

			@shopper = Shopper.find(:first, :conditions => ["userid = ? and password = ?",@userid, params[:password]])

			if (@shopper.nil?)
					redirect_to "/shoppers/login",:alert => "Your current session has expired. Please log out and login again."
					return
			end

			session["id"] = @shopper.id
			session[:user_role] = @shopper.role

			#@cart = Shoppingcart.find(:first, :conditions => ["shopper_id = ? and state = 'not_checked'",@shopper.id,@shopper.state])
			@cart = Shoppingcart.find(:first, :conditions => ["shopper_id = ? AND state is null",@shopper.id])
			if(!@cart.nil?)
				session["cart_id"]=@cart.id
			else
				@cart = Shoppingcart.new
				@cart.shopper_id = @shopper.id
				@shopper = Shopper.find_by_id(@shopper.id);
				@cart.retailer_id = @shopper.retailer_id
				#@cart.state = "not_checked"
				@cart.save
				session["cart_id"] = @cart.id
			end
			cookies.permanent.signed[:token] = [session[:id],session[:cart_id]];


			#else
			#	session["id"] = nil
			#	session["cart_id"] = nil
			#	redirect_to "/shoppers/login",:notice =>  'Login Unsuccessful!' 
		#end
		#redirect_to "/shoppers/login",:alert => "Invalid username/password"
	end
	
end
