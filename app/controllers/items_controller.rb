class ItemsController < ApplicationController
  layout "choose_onepg"
  # GET /items
  # GET /items.json
  def index
    @items = Item.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json =>  @items }
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show
    @item = Item.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json =>  @item }
    end
  end

  # GET /items/new
  # GET /items/new.json
  def new
    @item = Item.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @item }
    end
  end

  # GET /items/1/edit
  def edit
    @item = Item.find_by_id(params[:id])
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(params[:item])

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, :notice =>  'Item was successfully created.' }
        format.json { render :json =>  @item, :status => :created, :location=> @item }
      else
        format.html { render :action =>  "new" }
        format.json { render :json =>  @item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /items/1
  # PUT /items/1.json
  def update
    @item = Item.find_by_id(params[:id])

    respond_to do |format|
      if @item.update_attributes(params[:item])
        format.html { redirect_to @item, :notice =>  'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action =>  "edit" }
        format.json { render :json =>  @item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item = Item.find_by_id(params[:id])
    @item.destroy

    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end
  
	def new_item_from_product(prod)
		
		@item = Item.new
		@item.product_id = prod.id
		@item.item_subcateg_name = prod.subcategory
		@item.product_name = prod.product
		@item.brand_name = prod.brand
		@item.variant_name = prod.variant
		@item.item_uom = (prod.uom.nil? || prod.uom.index('{') != nil)? "pc" : prod.uom
		@item.item_size = prod.size
		@item.mrp = prod.mrp
		@item.merchant_price = prod.sellprice
		@item.Category_id = prod.cid	
		@item.anid = Product.lookup_anid(prod.id)
		
		return @item
	end
	
  def select
		
    @prod = Product.find_by_id(params[:id])
		
		@prod_anid = Product.lookup_anid(@prod.id)
		
		@cart_id = session["cart_id"]
		@item = Item.find(:first, :conditions => ["product_id = ? and cart_id = ?", params["id"], @cart_id]); 
		
		if(!@item.nil?)
			@tempamount = (params[:quantity]).to_f
			@tempamount2 = (@item.quantity).to_f
			@tempamount2 = @tempamount2 + @tempamount
			@item.quantity = @tempamount2.to_s
			@item.anid = @prod_anid
			@item.cart_id = @cart_id
			@item.save
			render :json => @item
		else
			if(session.has_key?("cart_id"))			#If the user is adding this item first time and he has logged in 
				@item = Item.new
				@item.cart_id = @cart_id
				@item.product_id = @prod.id
				@item.item_subcateg_name = @prod.subcategory
				@item.product_name = @prod.product
				@item.brand_name = @prod.brand
				@item.variant_name = @prod.variant
				@item.anid = @prod_anid
				@item.item_uom = (@prod.uom.nil? || @prod.uom.index('{') != nil)? "pc" : @prod.uom
				@item.item_size = @prod.size
				@item.mrp = @prod.mrp
				@item.merchant_price = @prod.sellprice
				@item.Category_id = @prod.cid
				@item.quantity = params[:quantity]
				@item.save
				render :json => @item
               
			else
				render :text =>"no"
			end
		end
  end
	
	def process_favorites(shopper_id)
		logger.debug "favorites of the shopper #{shopper_id}"
		
		@fav = Favorites.find(:first, :conditions => ["shopper_id=?", shopper_id])
		@fav_items = Hash.new
		
		logger.debug "favorite item found: #{@fav}"
		
		#todo: move this to a runner method to execute overnight for all shoppers
		if (@fav.nil?)
			logger.debug "Empty favorite, creating a new one"
			@fav = Favorites.new
			@fav.favorite_items = ActiveSupport::JSON.encode(Hash.new)
			@fav.shopper_id = session[:id]
		else
			logger.debug "Decoding favorite items #{@fav.favorite_items}"
			@fav_items = ActiveSupport::JSON.decode(@fav.favorite_items)
			logger.debug ">Favorites>>>>>>>>>>>>>>>>>>>>>>>: #{@fav_items}"
		end
		
		@time_ago = DateTime.now
		# in case of no favorite items, or if the favorite items update was 
		# more than 24 hours ago, recompute the favorite items again
		if(@fav_items.empty? || @fav.updated.nil? || @fav.updated < @time_ago) # 24.hours.ago)
			
			# logger.debug "reduce ranking of all existing favorite items"
			# # reduce ranking of all existing favorite items
			# @fav_items.each{ |prod_id, ranking| 
			# 	@fav_items[prod_id] -= (0.05 * @fav_items[prod_id])   # doubt  about the prod_id  , it must b ranking 
   #     }
			@days_earlier = DateTime.now-7
			logger.debug @days_earlier
			logger.debug '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
			if (@fav.updated.nil?)
				@days_earlier = 1000	# entire shopping history for the first time
			end
			
			logger.debug "Find all shopping carts of the shopper from a day ago"
			@shopper_carts = Shoppingcart.find(:all, :select => "id",:conditions => ["shopper_id = ? and created > ? and state = 'checked_out' ", shopper_id, @days_earlier],:order => "created desc") 
      logger.debug @shopper_carts.blank?
			if (!@shopper_carts.blank?)
				@shopper_cart_ids = @shopper_carts.map {|x| x.id}	#limit it to 20 - simpledb restriction
				@shopper_cart_ids = @shopper_cart_ids[0,18]
				logger.debug ">>>>>>>>>>>>>>>>>>>>>>>carts data"
				logger.debug "#{@shopper_cart_ids}"
				logger.debug "??????????????????????????"
				 #+ @shopper_cart_ids.count.to_s
				logger.debug "getting items from shopping carts #{@shopper_cart_ids}"
				
				logger.debug "Current favorites: #{@fav_items}"

				##["bf9d45a4-7f19-11e2-8350-12313810986b", "869d7d48-7f35-11e2-8350-12313810986b"]
				# my_array.map {|element| "'#{element}'"}.join(',')      # outputs a single string "'blue','green','red'" 
				@shopper_cart_ids_csv = @shopper_cart_ids.map {|element| "'#{element}'"}.join(',')
				#@shopped_items_list = Item.find(:all, :conditions => ["cart_id in ?", @shopper_cart_ids])	#does not work in earlier rails
				@shopped_items_list = Item.find(:all, :conditions => ["cart_id in ( #{@shopper_cart_ids_csv} )"])

				@shopped_items_list.each do |recent_item|
					logger.debug "#{recent_item}"
					if (@fav_items.has_key?(recent_item.product_id))
							@fav_items [recent_item.product_id] += 1
					else
						@fav_items[recent_item.product_id] = 1.0
					end

				end

				# sort them by ranking, and get only the first 20, in descending order of ranking
				@fav_items = @fav_items.sort_by {|_key, value| -value}
				
				#that returns an array so convert that to a hash
				@fav_items = Hash[@fav_items]
				
				logger.debug "Updated favorites: #{@fav_items}"
				@fav.favorite_items =  ActiveSupport::JSON.encode(@fav_items)
			else
				logger.debug "No shopping carts of the shopper from over a day"
			end
			
			#whether empty or not, do a save to mark the updated time to now
			@fav.save
			
			logger.debug "saved new favorites: #{@fav.favorite_items}"
			
		else
			logger.debug "Using already existing favorites"
		end
		
		#return only the top 20
		@fav_items = @fav_items.first(20)
		#that gets an array so convert that to a hash
		@fav_items = Hash[@fav_items]
		
		return @fav_items
		
	end
	
	def favorites
# ADDED BY MANDAR
 		@sorted_fav_products = []
 		@users_fav_items = {}

		# gets the list of product ids that are current favorites
		#@fav_pids = process_favorites(session[:id])
		# get shoppers top 20 fav item in desc rank order 
		@fav_data = Favorites.select(:all, :conditions => ["shopper_id = ?",session[:id]], :order => 'rank desc', :limit => 20)
		
		#get array of fav product ids
		@fav_pids = @fav_data.map{|item| item.product_id}

		#logger.debug("fav pids = #{@fav_pids.keys}")
		# get the list of items of the products that these favorites belong to
		@fav_prods = Product.find(:all, :conditions => ["id in (?)", @fav_pids])
		
		# converts the products into item objects
		@fav_prods.each do |fav_prod|
			@item = new_item_from_product(fav_prod)
			@users_fav_items[fav_prod.id.to_s] = @item
		end
		@fav_pids.each do |product_id|
			@sorted_fav_products << @users_fav_items[product_id].as_json
		end
	
		render :json => @sorted_fav_products
		#added by mandar
	end
	
  def checkcart
		logger.debug "checkcart: #{params}"
		if(params.has_key?("cart_id"))				#Can also use: if("cid" in params)
			@item = Item.find(:all, :conditions => ["cart_id = ? ", params["cart_id"]]); 
		end
		logger.debug @item
		if params.has_key?(:cart_id)
			render :json =>  @item 
		end
  end
  def remove
    @cart_id = session["cart_id"]
    @item = Item.find(:first, :conditions => ["product_id = ? and cart_id = ?", params[:product_id], @cart_id]);
    @item.destroy
		render :text =>"ok"
  end
end
