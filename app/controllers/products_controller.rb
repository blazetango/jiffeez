class ProductsController < ApplicationController
  layout "choose_onepg"
  # GET /products
  # GET /products.json
  def index
    
		@flag_promo = nil
		@flag_active = nil
		
		if (params[:promo] == 't')
			@flag_promo = true
		else 
			if (params[:promo] == 'f')
				@flag_promo = false
			end
		end

		if (params[:active] == 't')
			@flag_active = true
		else 
			if (params[:active] == 'f')
				@flag_active = false
			end
		end

		puts "flag promo #{@flag_promo}"
		
		if (@flag_promo != nil) 
			@Product_Cnd = Product.where("is_promotion=?", (@flag_promo? 1: 0))
		elsif (@flag_active != nil) 
			@Product_Cnd = Product.where("is_active=?", (@flag_active? 1: 0))
		else
				@Product_Cnd = Product.where("1=1")
		end
		
		@products = @Product_Cnd.paginate(:page => params[:page], :per_page => 20)
 #added by mandar 
   @update_time = Categ.order('updated_at desc').first.updated_at.to_i
	    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find_by_id(params[:id])
  end

  # POST /products
  # POST /products.json
  def create
    @product = product.new(params[:product])

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, :notice =>  'product was successfully created.' }
        format.json { render :json => @product, :status => :created, :location=> @product }
      else
        format.html { render :action =>  "new" }
        format.json { render :json => @product.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find_by_id(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, :notice =>  'product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action =>  "edit" }
        format.json { render :json => @product.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find_by_id(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end
  def choose
		
		@page = 1			  # if page number isn't specified
		@per_page = 100	#	upper limit per page
		
		@flag_promo = nil

		if (params.has_key?("promo"))
			if (params[:promo] == 'true')
				@flag_promo = true
			else
				@flag_promo = false
			end
		end
		
		if (params.has_key?("page"))
			
			@page = params[:page]
			
			if (params.has_key?("per_page"))
				@per_page = params[:per_page]
			else
				@per_page = 6
			end

		end
		
		if (params.has_key?("page"))
			
			@page = params[:page]
			
			if (params.has_key?("per_page"))
				@per_page = params[:per_page]
			else
				@per_page = 6
			end
			
			logger.debug("Page number: #{params[:page]} - limiting per page: #{@per_page}")
			
		else
			logger.debug("No pagination - page 1, limit 100")
		end
			
		#@product = product
		#puts(params['cid'])
		if(params.has_key?("cid"))				#Can also use: if("cid" in params)
			@subcateg = Subcateg.paginate(:conditions => ["cid = ? ", params["cid"]], :order=>"subcid", 
				:page => @page, :per_page => @per_page );
		elsif(params.has_key?("subcid"))				#Can also use: if("subcid" in params)
			#if (@flag_promo.nil?)
			#	@Product_Cnd = Product.all
			#else
				@Product_Cnd = Product.where("is_active=?", (@flag_promo? 1: 0))
			#end
			@product = Product.paginate(:conditions => ["subcid = ?", params["subcid"]],:order=>"product,variant,size", 
				:page => @page, :per_page => @per_page );
		elsif(params.has_key?("subcategid"))				#Can also use: if("subcid" in params)
			@brand = Brand.paginate(:conditions => ["subcategid = ? ", params["subcategid"]],:order=>"6 desc", 
				:page => @page, :per_page => @per_page );
		elsif(params.has_key?("brandid"))				#Can also use: if("subcid" in params)
			@products = Product.paginate(:conditions => ["brandid = ? AND is_active=1 AND uom is not null", params["brandid"]], 
			#@products = Product.paginate(:conditions => ["brandid = ? AND uom is not null", params["brandid"]], 
				:order => "product,variant", :order => "rank desc, sellprice desc",
				:page => @page, :per_page => @per_page );
		else
			@categs = Categ.paginate(:order => "cid", 
				:page => @page, :per_page => @per_page );
		end
		
		#	puts ActiveSupport::JSON.encode(@brand);
    respond_to do |format|
			if params.has_key?(:cid)
				format.html # choose.html.erb
				format.json { render :json => @subcateg }
			elsif params.has_key?(:subcid)
				format.html # choose.html.erb
				format.json { render :json => @product }
			elsif params.has_key?(:subcategid)
				format.html # choose.html.erb
				format.json { render :json => @brand }
			elsif params.has_key?(:brandid)
				format.html # choose.html.erb
				format.json { render :json => @products }
			else
				format.html # choose.html.erb
				format.json { render :json => @categs }	 
			end
		end
  end
	
	

	
end
