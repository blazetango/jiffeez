class SubcategsController < ApplicationController
	layout "choose_onepg"
  # GET /subcategs
  # GET /subcategs.json
  def index
    @subcategs = Subcateg.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json=> @subcategs }
    end
  end

  # GET /subcategs/1
  # GET /subcategs/1.json
  def show
    @subcateg = Subcateg.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json=> @subcateg }
    end
  end

  # GET /subcategs/new
  # GET /subcategs/new.json
  def new
    @subcateg = Subcateg.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json=> @subcateg }
    end
  end

  # GET /subcategs/1/edit
  def edit
    @subcateg = Subcateg.find_by_id(params[:id])
  end

  # POST /subcategs
  # POST /subcategs.json
  def create
    @subcateg = Subcateg.new(params[:subcateg])

    respond_to do |format|
      if @subcateg.save
        format.html { redirect_to @subcateg, :notice=> 'Subcateg was successfully created.' }
        format.json { render :json=> @subcateg, :status=> :created, :location=> @subcateg }
      else
        format.html { render :action=> "new" }
        format.json { render :json=> @subcateg.errors, :status=> :unprocessable_entity }
      end
    end
  end

  # PUT /subcategs/1
  # PUT /subcategs/1.json
  def update
    @subcateg = Subcateg.find_by_id(params[:id])

    respond_to do |format|
      if @subcateg.update_attributes(params[:subcateg])
        format.html { redirect_to @subcateg, :notice=> 'Subcateg was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action=> "edit" }
        format.json { render :json=> @subcateg.errors, :status=> :unprocessable_entity }
      end
    end
  end

  # DELETE /subcategs/1
  # DELETE /subcategs/1.json
  def destroy
    @subcateg = Subcateg.find_by_id(params[:id])
    @subcateg.destroy

    respond_to do |format|
      format.html { redirect_to subcategs_url }
      format.json { head :no_content }
    end
  end
end
