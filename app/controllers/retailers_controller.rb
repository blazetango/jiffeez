class RetailersController < ApplicationController
  layout "choose_onepg"
  # GET /retailers
  # GET /retailers.json
  def index
    @retailers = Retailer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @retailers }
    end
  end

  # GET /retailers/1
  # GET /retailers/1.json
  def show
    @retailer = Retailer.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @retailer }
    end
  end

  # GET /retailers/new
  # GET /retailers/new.json
  def new
    @retailer = Retailer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @retailer }
    end
  end

  # GET /retailers/1/edit
  def edit
    @retailer = Retailer.find_by_id(params[:id])
  end

  # POST /retailers
  # POST /retailers.json
  def create
    @retailer = Retailer.new(params[:retailer])

    respond_to do |format|
      if @retailer.save
        format.html { redirect_to @retailer, :notice => 'Retailer was successfully created.' }
        format.json { render :json => @retailer, :status => :created, :location => @retailer }
      else
        format.html { render :action => "new" }
        format.json { render :json => @retailer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /retailers/1
  # PUT /retailers/1.json
  def update
    @retailer = Retailer.find_by_id(params[:id])

    respond_to do |format|
      if @retailer.update_attributes(params[:retailer])
        format.html { redirect_to @retailer, :notice => 'Retailer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @retailer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /retailers/1
  # DELETE /retailers/1.json
  def destroy
    @retailer = Retailer.find_by_id(params[:id])
    @retailer.destroy

    respond_to do |format|
      format.html { redirect_to retailers_url }
      format.json { head :no_content }
    end
  end

	def delivery_times
		
		#puts "Value is #{params[:id]}"
		@shopper = Shopper.find_by_id(session[:id])

		if (@shopper.nil?)
			# "Your current session has expired. Please log out and login again."
			render :text => "no"
			return
		end
		
		@retailer = Retailer.find_by_id(@shopper.retailer_id)  


		@dt = @retailer.deliveryTime
		if (@dt.nil?)
			@dt = ""
		end
		
		if (!@dt.index("{hour").nil?)
			@dt = @dt.gsub("hour", "\"hour\"").gsub("min","\"min\"")
		end
		
		puts "Value is #{@dt}"
#added by mandar
    @delivery_delay = @retailer.delivery_delay
    @result = {"delivery_time" => @dt, "delivery_delay" => @delivery_delay}
		#already in json
    render :json => @result.as_json
#added by mandar
	end
	
	
  def orders
		
		@id = session[:id]
		@user = Shopper.find_by_id(@id)
		
		if(@user.nil?)
			format.html
      format.json { render :json => "no" }
			return
			
		else
			
			logger.debug " User: #{@user.userid} -- #{@user.role}"
			# ensure the user does not specify any other retailer if not admin
			if (!@user.is_admin)
				logger.warn("User is not an admin - switching to current retailer id")
				@id = @user.retailer_id
			end
			
			if (!@user.is_admin && !@user.is_retailer)
				logger.error "Invalid role: TODO: put up an error message " #TODO: ERror message
				return
			end
		end

    @retailer = Retailer.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.json { render :json => @retailer }
    end
  end
# changes done by mandar 
  def order_sent 
    @cart_id = params[:cart_id]
    @retailer_id = Shopper.find_by_id(session[:id]).retailer_id
    @flag = params[:flag]
    @status = Shoppingcart.mark_order_as_sent(@cart_id,@retailer_id,@flag)
    render :status => 200, :json => @status
  end

  def order_delivered 
    @cart_id = params[:cart_id]
    @retailer_id = Shopper.find_by_id(session[:id]).retailer_id
    @status = Shoppingcart.mark_order_as_delivered(@cart_id,@retailer_id)
    render :status => 200, :json => @status
  end

  def undo_order_delivery
    @cart_id = params[:cart_id]
    @retailer_id = Shopper.find_by_id(session[:id]).retailer_id
    @status = Shoppingcart.undo_order_delivery(@cart_id,@retailer_id)
    render :status => 200, :json => @status
  end

end
