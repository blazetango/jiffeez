class ShoppersController < ApplicationController
	
	include SmsMsg
	
	MAX_AUTH_RETRIES = 3	
	
  layout "choose_onepg"
  # GET /shoppers
  # GET /shoppers.json
  def index
    @shoppers = Shopper.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json =>  @shoppers }
    end
  end

  # GET /shoppers/1
  # GET /shoppers/1.json
  def show
    @shopper = Shopper.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json =>  @shopper }
    end
  end

  # GET /shoppers/new
  # GET /shoppers/new.json
  def new
    @shopper = Shopper.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @shopper }
    end
  end

  # GET /shoppers/1/edit
  def edit
    @shopper = Shopper.find_by_id(params[:id])
		
		if (@shopper.nil?)
			redirect_to "/shoppers/login",:alert => "Your current session has expired. Please log out and login again."
			return
		end
  end
	
	def copy
    @shopper_orig = Shopper.find_by_id(params[:id])	
		logger.debug("Shopper orig: #{@shopper_orig}")
		if (@shopper_orig.nil?)
				redirect_to "/shoppers/login",:alert => "Your current session has expired. Please log out and login again."
				return
			end
			
		#@shopper = Shopper.new(params[:id])
		#logger.debug("mew id #{@shopper.id}")
		@shopper = @shopper_orig.dup
		@shopper.id = nil
		
  end

  # POST /shoppers
  # POST /shoppers.json
  def create
		
		logger.debug "creating new shopper: #{params[:shopper]}"
    
		@result = { :status => 'created', :message => 'shopper was created' }
		
		@shopper = Shopper.new(params[:shopper])

		@oldshopper = Shopper.find(:first, :conditions => ["userid = ? ",@shopper.userid ])
		
		if @oldshopper == nil
			
			if (@shopper.is_userid_mobile)
				@result = { :status => 'error', :message => "Shopper with specified userid #{@shopper.userid} has to be verified first" }
				render :json => @result 
				return
			end
			
			logger.debug ("shopper does not exist, creating new")
			@shopper.save
			respond_to do |format|
				format.html { redirect_to "/shoppers", :notice =>  'Shopper was successfully created.' }
				format.json { render :json =>  @result } #@shopper, :status => :created, :location=> @shopper }
			end
		else
			
			if (@oldshopper.is_userid_mobile)
				
				if (@oldshopper.status == 'verified')
					
					@oldshopper.name = @shopper.name   
					@oldshopper.mobile = @shopper.mobile
					@oldshopper.email = @shopper.email
					@oldshopper.address = @shopper.address
					@oldshopper.userid = @shopper.userid
					@oldshopper.password = @shopper.password
					@oldshopper.location = @shopper.location
					@oldshopper.role = @shopper.role
					
					
					@default_retailer = Retailer.find_by_name('Test Bazaar')
					if (!@default_retailer.nil?)
						@oldshopper.retailer_id = @default_retailer.id
					end
					@oldshopper.status = 'ready'
					
					@oldshopper.save
					
					@result = { :status => 'created', :message => 'Shopper was created' }
					
				else
					
					@result = { :status => 'error', :message => "Shopper with specified userid #{@shopper.userid} should have status 'verified' and not #{@oldshopper.status}" }
					
				end
				
				render :json => @result 
				return

			end

			# the usual flow for alphanumeric ids
			logger.debug ("shopper already exists - not creating")
			@result[:status] = 'error'
			@result[:message] = "Shopper with specified userid #{@shopper.userid} already exists"
			
		  respond_to do |format|
				format.html { redirect_to "/shoppers/new", :notice =>  'Shopper was not created.' }
				format.json { render :json => @result } # @shopper.errors, :status => :unprocessable_entity }
		  end
		end
  end

  # PUT /shoppers/1
  # PUT /shoppers/1.json
  def update
    @shopper = Shopper.find_by_id(params[:id])
		
		if (@shopper.nil?)
			redirect_to "/shoppers/login",:alert => "Your current session has expired. Please log out and login again."
			return
		end

    respond_to do |format|
      if @shopper.update_attributes(params[:shopper])
        format.html { redirect_to @shopper, :notice =>  'Shopper was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action =>  "edit" }
        format.json { render :json =>  @shopper.errors, :status => :unprocessable_entity }
      end
    end
  end
	
# just a one time operation to update all date formats for delivery time
# probably will never be used again
	def archive_update_alldates
		#delivery_time
		
		logger.debug ("getting orders... #{session[:id]} -- #{session["cart_id"]}")
		@retailer_id = Shopper.find_by_id(session[:id]).retailer_id
		
		@shoppingcarts = Shoppingcart.find(:all, :conditions => ["delivery_time is not null"])
		
		@shoppingcarts.each_with_index do |cart, i|
			
			begin
				@del_date = DateTime.parse(cart.delivery_time) #.in_time_zone('Kolkata')
			rescue
				logger.debug "changing date to now"
				@del_date = DateTime.now #.in_time_zone('Kolkata')
			end
			
			cart.delivery_time = "#{@del_date}"
			cart.save
			
			logger.debug "#{cart.short_id} -- date: #{cart.delivery_time} ---- parsed as: #{@del_date}"
		end
			
	end
	
  def getorders
		
		@current_shoppingcart = Shoppingcart.find_by_id(params[:cart_id])
		@shoppingcarts = Shoppingcart.find(:all,
			:order => "updated desc",
			:conditions => ["(state is not null OR state != ?) AND shopper_id = ?", "", @current_shoppingcart.shopper_id]);
		
		puts ActiveSupport::JSON.encode(@shoppingcarts)
		
		render :json => @shoppingcarts;
		
  end

  # DELETE /shoppers/1
  # DELETE /shoppers/1.json
  def destroy
    @shopper = Shopper.find_by_id(params[:id])
		if (@shopper.nil?)
			format.html { redirect_to "/shoppers/login" }
			format.json { head :no_content }
			return
		end
    @shopper.destroy

    respond_to do |format|
      format.html { redirect_to shoppers_url }
      format.json { head :no_content }
    end
  end
	
	def get_shopper_login_url (shopper)
		if (shopper.is_admin) 
			logger.debug "Redirecting to admin - all retailers page"
			return "/retailers"
			#		render "/products/choose"
		elsif (shopper.is_retailer)
			logger.debug "Redirecting to retailer page"
			return "/retailers/#{@shopper.retailer_id}/orders"
			#		render "/products/choose"
		else
			logger.debug "Redirecting to products page"
			return "/products/choose#categpage"
			#		render "/products/choose"
		end
	end
 
	# Status returned can be one of:
	# ok - user logged in
	# ok_loggedin - user already logged in
	# err_password - password mismatch
	# err_userid_none: User id does not exist
	# err_userid_block: User id has been blocked due to too many retries and must register again
  def login_check
		
		# result to be returned as the ajax response in json
		@result =  { :status => 'ok', :message => 'Login succeeded', :url => '' }
		
		@userid = params[:userid]
		logger.debug "Login Check for userid: #{@userid}"
		
		# if user is empty, check if user is already logged in, 
		# and a valid session id exists
		if (params[:userid].blank?)
			@shopperid = session[:id]
			
			if(!session[:id].blank?)
				logger.debug "Session id is #{@shopperid}"
				@shopper = Shopper.find_by_id(@shopperid)
				
				if (@shopper.nil?)
					logger.debug "No shopper by that id"
					@result[:status] = 'err_notloggedin'
					@result[:message] = "No user already logged in, must sign in"
					@result[:url] = "/shoppers/login"
					render :json => @result
					return
				else
					logger.debug "Shopper logged in"
					@result[:status] = 'ok_loggedin'
					@result[:message] = "User already logged in as #{@shopper.userid}"
					@result[:url] = get_shopper_login_url (@shopper)
					logger.debug "Shopper URL: #{@result[:url]}"
					render :json => @result
					return
				end
			else
				logger.debug "Shopper not logged in"
				@result[:status] = 'err_notloggedin'
				@result[:message] = "No user already logged in, must sign in"
				@result[:url] = "/shoppers/login"
				render :json => @result
				return
			end
		else	# user id not empty, check credentials

			@userid = params[:userid].strip
			logger.debug "Login Check for user id #{@userid}"
		
			if (params[:password].empty?)
				logger.debug "Password empty"
				@result[:status] = 'err_password'
				@result[:message] = "Invalid password"
				@result[:url] = "/shoppers/login"
				render :json => @result
				return
			end
			
			@shopper = Shopper.find_by_userid(@userid)
		
			if (@shopper.nil?)
					@result[:status] = 'err_userid_none:'
					@result[:message] = "Userid does not exist"
					@result[:url] = "/shoppers/login"
					render :json => @result
					return
			else
				
				# if user id is a mobile number
				if (@shopper.is_userid_mobile)
					logger.debug "Login Check for mobilenumber, checking ready state"
					if (@shopper.status != 'ready')
						@result[:status] = 'err_usernotready'
						@result[:message] = "User has not completed mobile verification"
						@result[:url] = "/shoppers/login"
						logger.debug "Login Check for mobilenumber, failed, user not in ready state, redirecting to login"
						render :json => @result
						return
						# "Your mobile number needs to be registered and verified first."
					end

					logger.debug "Login Check for mobilenumber OK"
				end
				
				if (@shopper.password != params[:password])
					logger.debug "Password mismatch"
					
					@result[:status] = 'err_password'
					@result[:message] = "Invalid password"
					@result[:url] = "/shoppers/login"
					render :json => @result
					return
				else
					logger.debug "Password matched"
				end
			end
		end

		logger.debug("Has passed through all the checks so far")
		session["id"] = @shopper.id
		session[:user_role] = @shopper.role
		
		#@cart = Shoppingcart.find(:first, :conditions => ["shopper_id = ? and state = 'not_checked'",@shopper.id,@shopper.state])
		@cart = Shoppingcart.find(:first, :conditions => ["shopper_id = ? AND state is null",@shopper.id])
		if(!@cart.nil?)
			session["cart_id"]=@cart.id
		else
			@cart = Shoppingcart.new
			@cart.shopper_id = @shopper.id
			@shopper = Shopper.find_by_id(@shopper.id);
			@cart.retailer_id = @shopper.retailer_id
			#@cart.state = "not_checked"
			@cart.save
			session["cart_id"] = @cart.id
		end
		cookies.permanent.signed[:token] = [session[:id],session[:cart_id]];
		
		logger.debug("Saved cookie returning ok")
		
		@result[:status] = 'ok'
		@result[:message] = "User logged in"
		@result[:url] = get_shopper_login_url (@shopper)
		logger.debug "Shopper URL: #{@result[:url]}"
		
		render :json => @result
		
end

	# Status returned can be one of:
	# ok - user logged in
	# ok_loggedin - user already logged in
	# err_password - password mismatch
	# err_userid_none: User id does not exist
	# err_userid_block: User id has been blocked due to too many retries and must register again
  def mobile_remind_password
		
		# result to be returned as the ajax response in json
		@result =  { :status => 'ok', :message => 'Password SMS Sent' }
		
		@userid = params[:userid]
		logger.debug "Login Check for userid: #{@userid}"
		
		# if user is empty, check if user is already logged in, 
		# and a valid session id exists
		if (@userid.blank?)
			render :json => { :status => 'err_userid_missing', :message => 'User ID needs to be specified' }
			return
		end
		
		@userid = params[:userid].strip
		@shopper = Shopper.find_by_userid(@userid)
		
		if (@shopper.nil?)
			@result[:status] = 'err_userid_none:'
			@result[:message] = "Userid does not exist"
			render :json => @result
			return
		end
		
		if (!@shopper.is_userid_mobile)
			@result[:status] = 'err_userid_invalid'
			@result[:message] = "Userid has to be a valid mobile number"
			render :json => @result
			return
		end
				
		if (@shopper.status != 'ready')
			@result[:status] = 'err_usernotready'
			@result[:message] = "User has not completed mobile verification"
			logger.debug "Login Check for mobilenumber, failed, user not in ready state"
			render :json => @result
			return
			# "Your mobile number needs to be registered and verified first."
		end

		@message = "Your password for Jiffeez is #{@shopper.password} and has been sent to you at your request"
	
		SmsMsg.send 9243778001, @shopper.userid, @message
		
		@result[:status] = 'ok'
		@result[:message] = "Password SMS Sent"
		render :json => @result
		
end
  
def login_check_ajax
	render :action => 'login_check_ajax.html.erb', :layout => false
end
	
def login
	#clear_session
end
  
def logout
	super ('You have logged out, please sign in again.')
end

# mobile_register creates a new shopper with the user id 999999999
# (or uses the same if one already exists, 
# and generates an auth_token to be used for verification later
# (within timeout and retries limit) 
# 
# Various values for shopper status:
# - init (mobile_register initial call)
# - verified (mobile_verify succesful)
# - ready (all details entered successfully)
def mobile_register

	@result = Hash.new

	if (!params.has_key?("number"))
		render :json => { :status => 'error', :message => 'number missing' }
		return
	end
	
	@number = params[:number]

	#if it matches then this will be non-nil
	if ((@number =~ /\A\d{10}\z/).nil?)
		@result[:message] = 'mobile number needs to be 10 digits'
		@result[:status] = 'err_invalid_number'
		render :json => @result
		return
	end
	
	@result[:message] = ''
	@result[:status] = 'ok'
	
	@shopper = Shopper.find_by_userid(@number)
	
	if (@shopper.nil?)
		@shopper = Shopper.new
		@shopper.userid = @number
		@shopper.status = 'init'
	else
		@result[:message] = 'This mobile number is already registered to some other user'
		
		if (@shopper.status == 'ready')
			@result[:status] = 'error'
			render :json => @result
			return
		end
		
	end
	
	# Generate a four digit random number between 1000 to 9999 for this shopper
	@auth_token = (1000..9999).to_a.choice # .sample #Random.rand(10000 - 1000) + 1000
	
	@shopper.auth_token = @auth_token
	@shopper.auth_token_timestamp = "#{DateTime.now}"
	@shopper.auth_token_retry_count = 0
	@shopper.save
	
	@message = "Verification Code for registration is #{@auth_token}. " +
							"This is valid for 10 mins from time of request. " +
							" Please do not share code with anyone."
	
	SmsMsg.send 9243778001, @number, @message
	
	render :json => @result

end

def mobile_verify
	
	if (!params.has_key?("number") || !params.has_key?("auth_token"))
		render :json => { :status => 'error', :message => 'number or auth token missing' }
		return
	end
	
	@result = Hash.new
	
	@result[:message] = 'mobile succesfully verified'
	@result[:status] = 'ok'
	
	@number = params[:number]
	@auth_token = params[:auth_token]
	
	#if it matches then this will be non-nil
	if ((@number =~ /\A\d{10}\z/).nil?)
		@result[:message] = 'mobile number needs to be 10 digits'
		@result[:status] = 'err_invalid_number'
	end
	
	@shopper = Shopper.find_by_userid(@number)
	
	if (@shopper.nil?)
		@result[:message] = 'mobile needs to register first'
		@result[:status] = 'err_not_registered'
		render :json => @result
		return
	end
	
	#else
	# assuming shopper status is init, else done anyway if (@shopper.status  == 'init'
	if (DateTime.parse(@shopper.auth_token_timestamp) < 10.minutes.ago)
		@result[:message] = 'auth key was generated more than 10 minute sago'
		@result[:status] = 'err_timeout'
		render :json => @result
		return
	end
	
	logger.debug "Retries: #{@shopper.auth_token_retry_count}"
	
	if (@shopper.auth_token_retry_count >= MAX_AUTH_RETRIES)
		@result[:message] = 'too many retries, register again'
		@result[:status] = 'err_retries'
		render :json => @result
		return
	end
	
	logger.debug "Auth token: #{@shopper.auth_token} -- #{@auth_token}"
	if (@shopper.auth_token != @auth_token)
		@result[:message] = 'auth key incorrect'
		@result[:status] = 'err_mismatch'
		
		@shopper.auth_token_retry_count += 1
		@shopper.save
	
		render :json => @result
		return
	end
	
	@shopper.status = 'verified'
	@shopper.save
		
	render :json => @result
		
end
end
