class ShoppingcartsController < ApplicationController
	layout "choose_onepg"
	#layout "application"
  # GET /shoppingcarts
  # GET /shoppingcarts.json
  def index
    @shoppingcarts = Shoppingcart.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json =>  @shoppingcarts }
    end
  end

  # GET /shoppingcarts/1
  # GET /shoppingcarts/1.json
  def show
    @shoppingcart = Shoppingcart.find_by_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json =>  @shoppingcart }
    end
  end
	
	# Returns the cart and all its items
	def get_cart_details
		
		@cart_details = {}
		
		@cart_items = Item.find(:all, :conditions => ["cart_id = ? ", params[:cart_id]]); 
		
		@shoppingcart = Shoppingcart.find_by_id(params[:cart_id])
		
		@cart_details = { :cart => @shoppingcart, :items => @cart_items }

    render :json =>  @cart_details
		
	end

  # GET /shoppingcarts/new
  # GET /shoppingcarts/new.json
  def new
    @shoppingcart = Shoppingcart.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @shoppingcart }
    end
  end

  # GET /shoppingcarts/1/edit
  def edit
    @shoppingcart = Shoppingcart.find_by_id(params[:id])
  end

  # POST /shoppingcarts
  # POST /shoppingcarts.json
  def create
    @shoppingcart = Shoppingcart.new(params[:shoppingcart])

    respond_to do |format|
      if @shoppingcart.save
        format.html { redirect_to @shoppingcart, :notice=> 'Shoppingcart was successfully created.' }
        format.json { render :json =>  @shoppingcart, :status => :created, :location=> @shoppingcart }
      else
        format.html { render :action =>  "new" }
        format.json { render :json =>  @shoppingcart.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /shoppingcarts/1
  # PUT /shoppingcarts/1.json
  def update
    @shoppingcart = Shoppingcart.find_by_id(params[:id])

    respond_to do |format|
      if @shoppingcart.update_attributes(params[:shoppingcart])
        format.html { redirect_to @shoppingcart, :notice=> 'Shoppingcart was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action =>  "edit" }
        format.json { render :json =>  @shoppingcart.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /shoppingcarts/1
  # DELETE /shoppingcarts/1.json
  def destroy
    @shoppingcart = Shoppingcart.find_by_id(params[:id])
    @shoppingcart.destroy

    respond_to do |format|
      format.html { redirect_to shoppingcarts_url }
      format.json { head :no_content }
    end
  end

  def checkout
  	logger.debug "/////////////////////////////////////#{params}"
    @shoppingcart = Shoppingcart.find(:first, :conditions => ["id = ?",params[:cart_id]])
    @shopper = Shopper.find(:first, :conditions => ["id = ?", @shoppingcart.shopper_id]);
		
		@itemcount = Item.find(:count, :conditions => ["cart_id = ?", params[:cart_id]]);
    
		@shoppingcart.final_price = @shoppingcart.merchant_price = params[:total]
		@shoppingcart.items_count = @itemcount
		logger.debug "Checkout: cart duration = #{@shoppingcart.updated} - #{@shoppingcart.created}"
		#@shoppingcart.duration = @shoppingcart.updated - @shoppingcart.created

		@shoppingcart.save
		 
 		logger.debug "shopping cart = #{@shoppingcart.updated}"
		
		@del_time = "#{DateTime.parse(params[:deliveryTime]) rescue (DateTime.now + 2.days)}" 

    logger.debug "del time = #{@del_time}"

		#	@actual_addr = @shoppingcart.local_addr	
		@actual_addr = @shopper.address
		logger.debug "Actual address: #{@actual_addr}"
		render :text => @actual_addr 
  end
  
  def placeorder
		
		#ensure consistent date format in db - so parse a time object 
		#and get the ruby string representation of it
		#defaults to current time in case there's any error by any improbable chance
		@del_time = "#{DateTime.parse(params[:deliveryTime]) rescue (DateTime.now + 2.days)}" 
    	@curr_cart = Shoppingcart.find_by_id(params[:cart_id])	
		@curr_cart.state = "checked_out"
		@curr_cart.delivery_address = params[:ship_addr];
		@curr_cart.delivery_time = @del_time
		#@shoppingcart.shipping_addr = params[:ship_addr]
		@curr_cart.save

		@new_cart = Shoppingcart.new
		@new_cart.shopper_id = @curr_cart.shopper_id
		@shopper = Shopper.find_by_id(@curr_cart.shopper_id);
		# update the shopper with the address
		@shopper.address = params[:ship_addr];
		@shopper.save
		
		@retailer_id = @new_cart.retailer_id = @shopper.retailer_id
		#@new_cart.state = "not_checked"
		@new_cart.save
		session["cart_id"] = @new_cart.id
		cookies.permanent.signed[:token] = [session[:id],session[:cart_id]];
		@retail = Retailer.find_by_id(@retailer_id)
		
		logger.info"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+@retail.id
		
		#TODO: get the carts from the database to ensure integrity
		#session["cart_id"] = ""		To prevent the user from reusing the checked-out cart
		#@getallcarts = Shoppingcart.find(:all, :conditions => ["shopper_id = ? and retailer_id = ?",
		#		@curr_cart.shopper_id, @retailer_id], :limit => 2, :order => "created desc")
		
		
		
		logger.debug "New Cart ID: #{@new_cart.id}"
		#get the current cart from the database again
		@curr_cart = Shoppingcart.find_by_id(@curr_cart.id)
		
		@getallcarts = [ @new_cart, @curr_cart]
		#Shoppingcart.find(:all, :conditions => ["id in ?",
			#	[@new_cart.id, @curr_cart.id]], :limit => 2, :order => "created desc")
		
		#@getallcarts = Shoppingcart.find(:all, :conditions => ["id in ?",
		#		[@new_cart.id, @curr_cart.id]], :limit => 2, :order => "created desc")
		
		#I can get all the carts by using the @shoppingcart.shopper_id to print the final prices.
		message = sprintf("Your COD order #%s was processed. Shopper-id %s, Address %s. Time %s", 		
		@curr_cart.short_id, @shopper.userid, @curr_cart.delivery_address, @curr_cart.get_time_range);
		# if @retail.id = "76a0c6cc-97bb-11e2-a061-123141000ec9"
		# 	SmsMsg.send 9243778001, 9886882929, message
		# 	SmsMsg.send 9243778001, 9820550824 , message
  #   else 
  #     SmsMsg.send 9243778001, 9886882929, message
		# 	SmsMsg.send 9243778001, 9820550824 , message
		# 	SmsMsg.send 9243778001, 9972011248, message
  #     SmsMsg.send 9243778001, 9663663983, message
  #   end

		
		@getallcarts.each { |x| 
			logger.debug "Cart: #{x.id} - #{ActiveSupport::JSON.encode(x)}"
			#if ( x.delivery_time.nil? || x.delivery_time.blank?)
			#	logger.debug "x.delivery_time was blank"
			#	x.delivery_time = DateTime.now
			#end
		}
		#added by mandar
		Favorites.delay.update_rank(@curr_cart)

		render :json => @getallcarts
  end

  # this is meant for retailers t oget all order
  def getorders
		
		logger.debug ("getting orders... #{session[:id]} -- #{session["cart_id"]}")
		@retailer_id = Shopper.find_by_id(session[:id]).retailer_id
		
		@shoppingcarts = Shoppingcart.find(:all,:conditions => ["(state is not null OR state != ?) AND retailer_id = ?", "", @retailer_id],
			:order => "delivery_time desc");
		render :json => @shoppingcarts;
  end
	


  def processOrder
		
		@updated_cart_details = params[:cart_details]
		
    @shoppingcart = Shoppingcart.find(:first, :conditions => ["id = ?",params[:cart_id]])
		
		#@itemcount = Item.find(:count, :conditions => ["cart_id = ?", params[:cart_id]]);
		@orig_itemcount = @shoppingcart.items_count
		@curr_itemcount = @updated_cart_details[:cart][:items_count]
		
		@has_order_changed = (@shoppingcart.final_price != @updated_cart_details[:cart][:final_price] || @orig_itemcount != @curr_itemcount)
		
    @shopper = Shopper.find_by_id(@shoppingcart.shopper_id);
		@retailer_id = @shopper.retailer_id
		@shoppingcart.retailer_id = @retailer_id
    @shoppingcart.final_price = @updated_cart_details[:cart][:final_price]
		#merchant_price update only required for old orders before this field was added
		@shoppingcart.merchant_price = @updated_cart_details[:cart][:merchant_price] 
		@shoppingcart.items_count = @updated_cart_details[:cart][:items_count]
    @shoppingcart.state = "in-process"
		@shoppingcart.retailer_note = @updated_cart_details[:cart][:retailer_note]
		
    @shoppingcart.save
		
		@cart_items = Item.find(:all, :conditions => ["cart_id = ? ", params[:cart_id]]); 
		(0...@updated_cart_details[:items].length).each{|i| 
			
			@cart_items[i].is_retailer_removed = @updated_cart_details[:items][i.to_s][:is_retailer_removed]
			@cart_items[i].final_price = @updated_cart_details[:items][i.to_s][:final_price]
			@cart_items[i].save
			
			if (@cart_items[i].merchant_price != @cart_items[i].final_price)
				@has_order_changed = true
			end
		}
		
		# #72 : 3 of 5 items will be delivered between 3:30 -4:30 p.m. 
		# Items Unavailable- Musk Melon, Yummiez Hot Dogs. 
		# Cash on Delivery
		#
		message = sprintf("Your COD order #%s for Rs. %.2f was processed.",
											@shoppingcart.short_id, @shoppingcart.final_price);
							
		if (@curr_itemcount.to_i > 0)
			message += sprintf(" %d of %d items were available and will be delivered on %s. Please keep CASH ready on delivery.",
											@curr_itemcount, @orig_itemcount, @shoppingcart.get_time_range);
		else
			message += " None of the items you ordered are currently available. "
		end
		
		if (@has_order_changed)
			#message += "Details of items not available and any price reconciliations will accompany the invoice."
			message += " You can view details of unavailable items or price reconciliations in the My Orders section"
		else
			message += " You can view details of the order in the My Orders section"
		end
			
		#message = sprintf("Your COD order #%d was processed. %d of %d items will be delivered on %s. " + 
		#									"New order amount is Rs. %f. Please keep CASH ready on delivery.", 
		#								@shoppingcart.id, @itemcount, @itemcount, 
		#								@shoppingcart.delivery_time, @shoppingcart.final_price);
		
		SmsMsg.send 9243778001, @shopper.mobile, message

		redirect_to "/retailers/#{@retailer_id}/orders"
  end
	
#	def update_old_ids
#		logger.debug "Updating old ids"
#		@shoppingcarts = Shoppingcart.find(:all, :conditions => ["short_id is null"]);
#		@shoppingcarts.each_with_index do |cart, i|
#			cart.short_id = '%06i' % i
#			cart.save
#			logger.debug cart.short_id
#		end
#		logger.debug "Done!!"
#	end
end
