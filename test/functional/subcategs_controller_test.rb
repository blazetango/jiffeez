require 'test_helper'

class SubcategsControllerTest < ActionController::TestCase
  setup do
    @subcateg = subcategs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subcategs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subcateg" do
    assert_difference('Subcateg.count') do
      post :create, subcateg: @subcateg.attributes
    end

    assert_redirected_to subcateg_path(assigns(:subcateg))
  end

  test "should show subcateg" do
    get :show, id: @subcateg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subcateg
    assert_response :success
  end

  test "should update subcateg" do
    put :update, id: @subcateg, subcateg: @subcateg.attributes
    assert_redirected_to subcateg_path(assigns(:subcateg))
  end

  test "should destroy subcateg" do
    assert_difference('Subcateg.count', -1) do
      delete :destroy, id: @subcateg
    end

    assert_redirected_to subcategs_path
  end
end
