require 'test_helper'

class DataschemasControllerTest < ActionController::TestCase
  setup do
    @dataschema = dataschemas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dataschemas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dataschema" do
    assert_difference('Dataschema.count') do
      post :create, dataschema: @dataschema.attributes
    end

    assert_redirected_to dataschema_path(assigns(:dataschema))
  end

  test "should show dataschema" do
    get :show, id: @dataschema
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dataschema
    assert_response :success
  end

  test "should update dataschema" do
    put :update, id: @dataschema, dataschema: @dataschema.attributes
    assert_redirected_to dataschema_path(assigns(:dataschema))
  end

  test "should destroy dataschema" do
    assert_difference('Dataschema.count', -1) do
      delete :destroy, id: @dataschema
    end

    assert_redirected_to dataschemas_path
  end
end
