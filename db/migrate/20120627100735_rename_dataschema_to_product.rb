class RenameDataschemaToProduct < ActiveRecord::Migration
 def self.up
        rename_table :dataschemas, :products
      end
     def self.down
        rename_table :products, :dataschemas
     end
end
