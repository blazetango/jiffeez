class CreateTrackupdates < ActiveRecord::Migration
  def change
    create_table :trackupdates do |t|
      t.integer :product_id
      t.float :mrp
      t.float :sellprice
      t.date :update_date
      t.boolean :active

      t.timestamps
    end
  end
end
