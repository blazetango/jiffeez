class CreateMissingupdates < ActiveRecord::Migration
  def change
    create_table :missingupdates do |t|
      t.integer :product_id
      t.string :error

      t.timestamps
    end
  end
end
