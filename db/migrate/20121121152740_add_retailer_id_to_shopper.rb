class AddRetailerIdToShopper < ActiveRecord::Migration
  def change
    add_column :shoppers, :retailer_id, :integer

  end
end
