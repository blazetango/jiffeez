class CreateCategs < ActiveRecord::Migration
  def change
    create_table :categs do |t|
      t.integer :cid
      t.string :categname
      t.string :order
      t.references :Categ
      t.string :Product

      t.timestamps
    end
    add_index :categs, :Categ_id
  end
end
