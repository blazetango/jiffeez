class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :cart_id
      t.integer :product_id
      t.string :quantity
      t.float :mrp
      t.float :merchant_price
      t.float :discount
      t.float :final_price
      t.string :state

      t.timestamps
    end
  end
end
