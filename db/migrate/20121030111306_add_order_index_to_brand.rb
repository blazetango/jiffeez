class AddOrderIndexToBrand < ActiveRecord::Migration
  def change
	add_index :brands, :order
  end
end
