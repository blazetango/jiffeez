class CreateSubcategs < ActiveRecord::Migration
  def change
    create_table :subcategs do |t|
      t.integer :subcid
      t.integer :cid
      t.string :subcategname
      t.string :order
      t.references :Product
      t.string :Subcateg

      t.timestamps
    end
    add_index :subcategs, :Product_id
  end
end
