class CreateDataschemas < ActiveRecord::Migration
  def change
    create_table :dataschemas do |t|
      t.string :category
      t.string :subcategory
      t.string :product
      t.string :brand
      t.string :variant
      t.float :size
      t.float :mrp
      t.float :sellprice
      t.string :uom

      t.timestamps
    end
  end
end
