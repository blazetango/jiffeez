class ChangeFlagsInProduct < ActiveRecord::Migration
  def change
		
		change_column :products, :is_active, :integer
		change_column :products, :is_promotion, :integer
		change_column :subcategs, :is_promotion, :integer
		change_column :brands, :is_promotion, :integer
		
		puts "Changed column types"
		
		Product.all.each do |product|
      product.update_attributes! :is_active => 1, :is_promotion => 0
    end
		
		puts "Updated all products to active and not promoted"
		
		Subcateg.all.each do |sc|
			sc.update_attributes! :is_promotion => 0
		end
		
		puts "Updated all subcategs to not promoted"
		
		Brand.all.each do |brand|
			brand.update_attributes! :is_promotion => 0
		end
		
		puts "Updated all brands to not promoted"
		
	end
end
