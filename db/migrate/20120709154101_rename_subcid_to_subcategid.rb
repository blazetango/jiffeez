class RenameSubcidToSubcategid < ActiveRecord::Migration
  def self.up
		rename_column :brands, :subcid, :subcategid
  end

  def self.down
		rename_column :brands, :subcategid, :subcid
  end
end
