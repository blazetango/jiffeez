class AddFlagsToProduct < ActiveRecord::Migration
  def change
    
		add_column :products, :is_active, :boolean
    add_column :products, :is_promotion, :boolean
		
		Product.all.each do |product|
      product.update_attributes! :is_active => true, :is_promotion => false
    end

  end
	
end
