class CreateShoppingcarts < ActiveRecord::Migration
  def change
    create_table :shoppingcarts do |t|
      t.integer :shopper_id
      t.float :final_price
      t.time :duration
      t.float :discount
      t.float :mrp_price
      t.string :state

      t.timestamps
    end
  end
end
