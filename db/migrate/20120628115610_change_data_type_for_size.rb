class ChangeDataTypeForSize < ActiveRecord::Migration
 def self.up
    change_table :products do |t|
      t.change :size, :string
    end
  end

  def self.down
    change_table :products do |t|
      t.change :size, :float
    end
  end
end  