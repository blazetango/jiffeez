class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.integer :brandid
      t.integer :subcid
      t.integer :cid
      t.string :brandname
      t.string :order
      t.references :Brand
      t.string :Product

      t.timestamps
    end
    add_index :brands, :Brand_id
  end
end
