class CreateShoppers < ActiveRecord::Migration
  def change
    create_table :shoppers do |t|
      t.string :name
      t.string :mobile
      t.string :address
      t.string :userid
      t.string :password
      t.string :location

      t.timestamps
    end
  end
end
