# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130605132353) do

  create_table "brands", :force => true do |t|
    t.integer  "brandid"
    t.integer  "subcategid"
    t.integer  "cid"
    t.string   "brandname"
    t.string   "order"
    t.integer  "Brand_id"
    t.string   "Product"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "is_promotion"
  end

  add_index "brands", ["Brand_id"], :name => "index_brands_on_Brand_id"
  add_index "brands", ["order"], :name => "index_brands_on_order"
  add_index "brands", ["subcategid"], :name => "index_brands_on_subcategid"

  create_table "categs", :force => true do |t|
    t.integer  "cid"
    t.string   "categname"
    t.string   "order"
    t.integer  "Categ_id"
    t.string   "Product"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "categs", ["Categ_id"], :name => "index_categs_on_Categ_id"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "items", :force => true do |t|
    t.integer  "cart_id"
    t.integer  "product_id"
    t.string   "quantity"
    t.float    "mrp"
    t.float    "merchant_price"
    t.float    "discount"
    t.float    "final_price"
    t.string   "state"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "product_name"
    t.string   "brand_name"
    t.string   "variant_name"
    t.string   "item_uom"
    t.string   "item_size"
    t.string   "item_subcateg_name"
    t.integer  "Category_id"
  end

  create_table "missingupdates", :force => true do |t|
    t.integer  "product_id"
    t.string   "error"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "category"
    t.string   "subcategory"
    t.string   "product"
    t.string   "brand"
    t.string   "variant"
    t.string   "size"
    t.float    "mrp"
    t.float    "sellprice"
    t.string   "uom"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "cid"
    t.integer  "subcid"
    t.integer  "brandid"
    t.float    "rank"
    t.integer  "is_active"
    t.integer  "is_promotion"
  end

  add_index "products", ["brandid"], :name => "index_products_on_brandid"

  create_table "retailers", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "deliveryTime"
    t.integer  "weeklyException"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "phone_number"
  end

  create_table "shoppers", :force => true do |t|
    t.string   "name"
    t.string   "mobile"
    t.string   "address"
    t.string   "userid"
    t.string   "password"
    t.string   "location"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "retailer_id"
    t.string   "role"
  end

  create_table "shoppingcarts", :force => true do |t|
    t.integer  "shopper_id"
    t.float    "final_price"
    t.time     "duration"
    t.float    "discount"
    t.float    "mrp_price"
    t.string   "state"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "delivery_address"
    t.string   "delivery_time"
    t.integer  "retailer_id"
    t.integer  "items_count"
  end

  create_table "subcategs", :force => true do |t|
    t.integer  "subcid"
    t.integer  "cid"
    t.string   "subcategname"
    t.string   "order"
    t.integer  "Product_id"
    t.string   "Subcateg"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "is_promotion"
  end

  add_index "subcategs", ["Product_id"], :name => "index_subcategs_on_Product_id"
  add_index "subcategs", ["cid"], :name => "index_subcategs_on_cid"

  create_table "trackupdates", :force => true do |t|
    t.integer  "product_id"
    t.float    "mrp"
    t.float    "sellprice"
    t.date     "update_date"
    t.boolean  "active"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.boolean  "is_promotion"
    t.string   "variant"
    t.string   "size"
  end

end
